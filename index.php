<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<meta name="google-site-verification" content="0J0xefxt2gE7R15hYLzwsgx0eKXxVVs7tGiQN_inb3Y" />-->
    <title>Apollo Bartenders</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    
    <link href="owl/owl.carousel.css" rel="stylesheet">
    <link href="owl/owl.theme.css" rel="stylesheet">

    <link rel="stylesheet" href="css/logo-nav.css">
    <link rel="stylesheet" type="text/css" href="css/base_style_apollo.css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="icon" href="img/fviconapollo/apolloLogoFI.png">

</head>

<body>
    
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav visible-xs-block visible-sm-block" href="index" id="ApolloLMenu">APOLLO</a>
                <a class="navbar-brand topnav visible-md-block visible-lg-block" href="index" ><img src="img/apolloLogo3.png" alt="" style="width:7em;" id="imgLogo"></a>
                
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" id="ulNavApollo">
                    
                    <li>
                        <a href="#services">SERVIÇOS</a>
                    </li>

                    
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">COMO CONTRATAR?</a>
                      <ul class="dropdown-menu">
                        <li><a href="como_contratar#comoContratar">Escolhendo uma empresa</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="como_contratar#formOrcamentoS">Solicite seu orçamento</a></li>                
                      </ul>
                    </li>
                    
                    <li>
                        <a href="plano#plano">PLANO DE FIDELIDADE</a>
                    </li>


                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">A EMPRESA <!--<span class="caret"></span>--></a>
                      <ul class="dropdown-menu">
                        <li><a href="empresa#sobre">Sobre</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="empresa#missao">Missão, Visão, Valores</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="empresa#fundadores">Fundadores</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="empresa#fale">Fale Conosco</a></li>                
                      </ul>
                    </li>

                    <li>
                        <a href="drinks">GALERIA</a>
                    </li>

                    <li>
                        <a href="empresa#fale">FALE CONOSCO</a>
                    </li>

                    <li><a href="midias_sociais">MÍDIAS SOCIAIS</a></li>

                    

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    

    <!-- Header -->
    <a name="about"></a>
    <?php include_once "controller/functionsGerenciadorConteudoPDO.php"; ?>
    <?php 

        $data_index = getIndex();

    

        $path_img_header = substr($data_index[0]->path_imagem, 3);

    ?>
    <div class="intro-header" style="background: url(<?php echo $path_img_header; ?>) no-repeat center center;margin-top:3.5em;">      
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="intro-message">

                        <h1><?php echo $data_index[0]->titulo_banner;?></h1>
                        <h3><?php echo $data_index[0]->subtitulo_banner;?></h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <li>
                                <a href="<?php echo $data_index[0]->link_youtube; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-youtube"></i> <span class="network-name">Youtube</span></a>
                            </li>
                            <li>
                                <a href="<?php echo $data_index[0]->link_facebook; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-facebook"></i> <span class="network-name">Facebook</span></a>
                            </li>
                            <li>
                                <a href="<?php echo $data_index[0]->link_instagram; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-instagram"></i> <span class="network-name">Instagram</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->
    <a name="sobre"></a>
    <div class="container" >
        
    <div class="row">
        <div class="col-md-12">
            
            <div class="container_text" style="padding-top:10em;">
                <?php echo $data_index[0]->texto_de_apresentacao; ?>

            </div>


        </div>
     
    </div>

    </div>

    <a  name="services"></a>
    
    <?php

        $servicos = getAllServicos();
        if(isset($servicos)){
            foreach ($servicos as $key => $value) {

            if($key % 2 == 0)
            {
                $path_img = substr($value->path_imagem, 3);
                echo "<div class='content-section-a' style='padding-top:7em;'>

                <div class='container'>
                    <div class='row'>
                        <div class='col-lg-5 col-sm-6'><!--class='col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6'-->
                            <hr class='section-heading-spacer'>
                            <div class='clearfix'></div>
                            <h2 class='section-heading'>$value->titulo</h2>
                                <span class='ntext'>$value->descricao</span>
                        </div>
                        <div class='col-lg-5 col-lg-offset-2 col-sm-6 ntext'>

                            <img class='img-responsive img-circle img_service' src='$path_img' alt=''  onmouseover='hover(this);' onmouseout='unhover(this);' style='padding-top:3em;'>
                        </div>
                    </div>

                </div>
                <!-- /.container -->

            </div>
            

            </div>
            <!-- /.content-section-a -->";
            }
            else
            {
                $path_img = substr($value->path_imagem, 3);
                echo "<div class='content-section-a'>

                <div class='container'>
                    <div class='row'>
                        <div class='col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6'><!--class='col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6'-->
                            <hr class='section-heading-spacer'>
                            <div class='clearfix'></div>
                            <h2 class='section-heading'>$value->titulo</h2>
                            <span class='ntext'>$value->descricao</span>
                        </div>
                        <div class='col-lg-5 col-sm-pull-6  col-sm-6'>

                            <img class='img-responsive img-circle img_service' src='$path_img' alt=''  onmouseover='hover(this);' onmouseout='unhover(this);' style='padding-top:3em;'>
                        </div>
                    </div>

                </div>
                <!-- /.container -->

            </div>
            

            </div>
            <!-- /.content-section-a -->";
            }

            
        }
        }

    ?>
    
    <?php $data_footer = getRodape(); ?>
    
    <?php

        $path_img_footer = substr($data_footer[0]->path_imagem, 3);


    ?>
    <div class="banner" style="background: url(<?php echo $path_img_footer; ?>) no-repeat center center;"> 

        <div class="container">

            <div class="row">
                <div class="col-lg-6" id="text_footer">
                    <?php echo $data_footer[0]->texto_rodape1; ?>
                    <h2><?php echo $data_footer[0]->texto_rodape2; ?></h2>                  
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="<?php echo $data_index[0]->link_youtube; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-youtube"></i> <span class="network-name">Youtube</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $data_index[0]->link_facebook; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-facebook"></i> <span class="network-name">Facebook</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $data_index[0]->link_instagram; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-instagram"></i> <span class="network-name">Instagram</span></a>
                        </li>
                    </ul>


                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="index">Inicio</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="empresa#sobre">Sobre</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#services">Serviços</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="empresa#fale">Contato</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; Apollo 2015. Todos Direitos Reservados</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><span class="network-name">Plano de Fidelidade</span></h4>
          </div>
          <div class="modal-body">
            <!--<iframe width="100%" height="400em" src="https://www.youtube.com/embed/zhrb6znxDbw" frameborder="0" allowfullscreen></iframe><!--https://www.youtube.com/embed/zhrb6znxDbw-->
                <div class="embed-responsive embed-responsive-16by9">
                   
                    <?php echo $data_index[0]->url_youtube_video_explicativo; ?>
                </div>
          </div>
          <div class="modal-footer">
            <img src="img/drink_icons/cocktail13.png" alt="" class="left" width="41em;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            
          </div>
        </div>
      </div>
    </div>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    

    <!--esconder a div de valores e fundadores-->
    <script>
        $( ".sobre_div_valores" ).hide();
        $( ".sobre_div_fundadores" ).hide();
        $( ".sobre_div_plano" ).hide();
        $( ".sobre_div_como_contratar" ).hide();

        var flipV = 0;
        var flipF = 0;
        var flipP = 0;
        var flipC = 0;
        $( "#valores_id" ).click(function() {
          $( ".sobre_div_valores" ).toggle( flipV++ % 2 === 0 );
        });


        $( "#valores_id_fundadores" ).click(function() {
          $( ".sobre_div_fundadores" ).toggle( flipF++ % 2 === 0 );
        });

        $( "#valores_id_plano" ).click(function() {
          $( ".sobre_div_plano" ).toggle( flipP++ % 2 === 0 );
        });

         $( "#valores_id_como_contratar" ).click(function() {
          $( ".sobre_div_como_contratar" ).toggle( flipC++ % 2 === 0 );
        });

    </script>

    <script>

        function hover(element) {
            //element.setAttribute('src', 'img/drink3.png');
            //element.src='img/drink3.png';
        }
        function unhover(element) {
                    //element.setAttribute('src', 'img/drink4.png');
                    //element.src='img/drink4.png';
        }

    </script>
    

    <script src="owl/owl.carousel.js"></script>


    <script>

        $(document).ready(function() {
 
            $("#owl-demo").owlCarousel({
     
              navigation : true, // Show next and prev buttons
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem:true
         
              // "singleItem:true" is a shortcut for:
              // items : 1, 
              // itemsDesktop : false,
              // itemsDesktopSmall : false,
              // itemsTablet: false,
              // itemsMobile : false
         
             });
 
        });

    </script>

    
    <!--form-->
    <script>
       
       
       $( "#switch_form" ).click(function() {
            $( "#result" ).empty();
            $( ".form_togle" ).toggle();
            $(".formName").toggle();
           
        });

    </script>
 

<!--script close video modal-->
<script>
    
    $("#myModal").on('hidden.bs.modal', function (e) {
    $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
    });
 


</script>

<script>

        $( "#formComumSend" ).submit(function( event ) {
      
          // Stop form from submitting normally
          event.preventDefault();
         
          // Get some values from elements on the page:
          var $form = $( this ),
            nameC = $form.find( "input[name='nameC']" ).val(),
            emailC = $form.find( "input[name='emailC']" ).val(),
            phoneC = $form.find( "input[name='phoneC']" ).val(),
            messageC = $form.find( "textarea[name='messageC']" ).val(),
            url = $form.attr( "action" );
            
          // Send the data using post
          var posting = $.post( url, { nameC: nameC, phoneC: phoneC, emailC: emailC, messageC: messageC } );
         
          // Put the results in a div
          posting.done(function( data ) {

            //var content = $( data ).find( "#content" );

            //if(data.localeCompare("session_error2")){//
                //window.location.reload(); 
                
            //}else{
                $( "#result" ).empty().append( data );
                //$( "#result" ).empty().append( data.localeCompare('session_error') );
                //$("#formUpdatePassword").trigger('reset');
                if(data.length > 95){
                    function relload(){
                        $("#formComumSend").trigger('reset');
                      window.location.reload();
                    }
                    setTimeout(relload, 3500);
                }

            //}
            
          });
        });

    </script>


<script>

        $( "#formOrcamentoSend" ).submit(function( event ) {
      
          // Stop form from submitting normally
          event.preventDefault();
            categoriaEvento_n = "";
            $('input[name="categoriaEvento"]:checked').each(function() {
               //alert(this.value); 
               categoriaEvento_n += (this.value +", ");
            });



          //data = $('input[name=dataEvento]').val();
        

          // Get some values from elements on the page:
          var $form = $( this ),
            name = $form.find( "input[name='name']" ).val(),
            email = $form.find( "input[name='email']" ).val(),
            phone = $form.find( "input[name='phone']" ).val(),
            phoneRes = $form.find( "input[name='phoneRes']" ).val(),
            categoriaEvento = categoriaEvento_n,
            numeroConvidados = $form.find( "input[name='numeroConvidados']" ).val(),
            rua = $form.find( "input[name='rua']" ).val(),
            dataEvento = $form.find( "input[name='dataEvento']" ).val(),
            numeroCasa = $form.find( "input[name='numeroCasa']" ).val(),
            horarioEvento = $form.find( "input[name='horarioEvento']" ).val(),
            optionsRadiosGarcons = $form.find( "input[name='optionsRadiosGarcons']:checked" ).val(),
            optionsRadiosDrinks = $form.find( "input[name='optionsRadiosDrinks']:checked" ).val(),
            optionsRadiosComoConheceu = $form.find( "input[name='optionsRadiosComoConheceu']:checked" ).val(),
            inputCommentSobreEventoOrcamento = $form.find( "textarea[name='inputCommentSobreEventoOrcamento']" ).val(),
            inputComoConheceuSobreEventoOrcamento = $form.find( "textarea[name='inputComoConheceuSobreEventoOrcamento']" ).val(),
            url = $form.attr( "action" );
            
          // Send the data using post
          var posting = $.post( url, { name:name,email:email,phone:phone,phoneRes:phoneRes,categoriaEvento:categoriaEvento,numeroConvidados:numeroConvidados,
          rua:rua,dataEvento:dataEvento,numeroCasa:numeroCasa,horarioEvento:horarioEvento,optionsRadiosGarcons:optionsRadiosGarcons,optionsRadiosDrinks:optionsRadiosDrinks,
          optionsRadiosComoConheceu:optionsRadiosComoConheceu,inputCommentSobreEventoOrcamento:inputCommentSobreEventoOrcamento,inputComoConheceuSobreEventoOrcamento:inputComoConheceuSobreEventoOrcamento } );
         
          // Put the results in a div
          posting.done(function( data ) {

            //var content = $( data ).find( "#content" );

            //if(data.localeCompare("session_error2")){//
                //window.location.reload(); 
                
            //}else{
                $( "#result" ).empty().append( data );
                //$( "#result" ).empty().append( data.localeCompare('session_error') );
                //$("#formUpdatePassword").trigger('reset');
                if(data.length > 95){
                    function relloadOr(){
                        $("#formComumSend").trigger('reset');
                      window.location.reload();
                    }
                    setTimeout(relloadOr, 3500);
                }

            //}
            
          });
        });

    </script>

    

    <script type="text/javascript">

        var password = document.getElementById("email")
        , confirm_password = document.getElementById("emailConfirm");

        function validatePassword(){
          if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Email diferente");
          } else {
            confirm_password.setCustomValidity('');
          }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;

    </script>


</body>

</html>
