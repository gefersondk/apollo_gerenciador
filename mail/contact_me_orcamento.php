<?php
// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['phone']) 		||
   empty($_POST['inputCommentSobreEventoOrcamento'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "<p class='alert alert-danger'>Dados inválidos!</p>";
	return false;
   }
require 'Mail/PHPMailerAutoload.php';

$name = $_POST['name'];
$email_address = $_POST['email'];
$phone = $_POST['phone'];
$phoneResidencial = $_POST['phoneRes'];
$categoriaEvento = $_POST['categoriaEvento'];
$numeroConvidados= $_POST['numeroConvidados'];
$dataEvento = $_POST['dataEvento'];
$rua = $_POST['rua'];
$numeroCasa = $_POST['numeroCasa'];
$horarioEvento = $_POST['horarioEvento'];
$optionsRadiosGarcons = $_POST['optionsRadiosGarcons'];
$optionsRadiosDrinks = $_POST['optionsRadiosDrinks'];
$inputCommentSobreEventoOrcamento = $_POST['inputCommentSobreEventoOrcamento'];
$optionsRadiosComoConheceu = $_POST['optionsRadiosComoConheceu'];
$inputComoConheceuSobreEventoOrcamento = $_POST['inputComoConheceuSobreEventoOrcamento'];

//Create a new PHPMailer instance
$mail = new PHPMailer;
// Set PHPMailer to use the sendmail transport
$mail->isSendmail();
//Set who the message is to be sent from
$mail->setFrom($email_address, $name);
//Set an alternative reply-to address
$mail->addReplyTo($email_address, $name);
//Set who the message is to be sent to 
$mail->addAddress('eventos@apollobartenders.com.br', 'Apollo');
//Set the subject line
$mail->Subject = $name." ".$email_address;

$mail->IsHTML(true);

$dataEvento =  date('d-m-y',strtotime($dataEvento));

$mFormat= "Nome: %s <br>Email: %s <br>Telefone: %s <br>Telefone residencial: %s <br>Categoria do evento: %s <br>Numero de convidados: %s <br>Data do evento: %s <br>Rua: %s <br>Numero da casa: %s <br>Horário do evento: %s <br> 
Serviço de garçons servindo bebidas alcoólicas como champagne e cerveja: %s <br>Quais tipos de drinks e coquetéis: %s <br>Conte-nos mais sobre o seu evento: %s <br>  
Como você conheceu a Apollo Bartenders: %s %s <br>";
$mFinal = sprintf($mFormat, $name, $email_address, $phone, $phoneResidencial, $categoriaEvento,$numeroConvidados,$dataEvento,$rua,$numeroCasa,$horarioEvento,$optionsRadiosGarcons,$optionsRadiosDrinks,
   $inputCommentSobreEventoOrcamento, $optionsRadiosComoConheceu,$inputComoConheceuSobreEventoOrcamento);

$mail->Body='Nome:  '.$_POST['name'].'<br />
Email:  '.$_POST['email'].'<br />
<br /><br />

'.nl2br($mFinal).'

<br /><br /> 
Browser:  '.$_SERVER['HTTP_USER_AGENT'].'<br />
IP:  '.$_SERVER['REMOTE_ADDR'].'<br />
';

if (!$mail->send()) {
   echo "<p class='alert alert-danger'>Falha ao enviar!</p>";
}else{
   
   echo "<p class='alert alert-success'>Solicitação enviada com sucesso!. Entraremos em contato com você novo.</p>";
}

//echo $optionsRadiosDrinks."<br>";
//echo $optionsRadiosGarcons."<br>";



//echo $dataEvento;
//echo $dataEvento;



//echo $mFinal;
	//laracruz.paulo@gmail.com
// Create the email and send the message
//$to = 'laracruz.paulo@gmail.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
//$email_subject = "Contato via site:  $name";
//$email_body = $mFinal;//"Você recebeu uma mensagem, aqui estão os detalhes.\n\nNome: $name\n\nEmail: $email_address\n\nTelefone: $phone\n\nMensagem:\n$message";
//$headers = "From: ".$email_address."\r\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
//$headers .= "Reply-To:" .$email_address."\r\n";	
//mail($to,$email_subject,$email_body,$headers);
//echo "<p class='alert alert-success'>Solicitação enviada com sucesso!. Entraremos em contato com você.</p>";
//return true;	


?>