/*CREATE DATABASE apollo_data;*/

USE apollo_data;

CREATE TABLE Usuario_Apollo (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,

nome VARCHAR(200) NOT NULL,
email VARCHAR(200) ,
login VARCHAR(200) NOT NULL,
senha VARCHAR(500) NOT NULL,
path_imagem_usuario VARCHAR(300),
path_imagem_figurino_thumbnail VARCHAR(200)
) ;


CREATE TABLE Home (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
titulo_banner VARCHAR(500) ,
subtitulo_banner VARCHAR(500) ,
texto_de_apresentacao  TEXT,
texto_valores TEXT,
texto_missao TEXT,
texto_fundadores TEXT,
texto_plano_de_fidelidade TEXT,
url_youtube_video_explicativo TEXT,
texto_como_contratar TEXT,
texto_formulario_contato_comum TEXT,
texto_formulario_contato_orcamento TEXT,
path_imagem VARCHAR(500),
link_instagram VARCHAR(500),
link_youtube VARCHAR(500),
link_facebook VARCHAR(500)
) ;



CREATE TABLE Rodape (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,

texto_rodape1 TEXT ,
texto_rodape2 TEXT ,
path_imagem VARCHAR(500) ,
path_imagem_thumbnail VARCHAR(500) 
) ;


CREATE TABLE Servico (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
titulo VARCHAR(500) ,
descricao TEXT ,
path_imagem VARCHAR(500) ,
path_imagem_thumbnail VARCHAR(500) 
);

CREATE TABLE Galeria_Servico (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
titulo VARCHAR(500) ,
descricao TEXT ,
path_imagem VARCHAR(500) ,
path_imagem_thumbnail VARCHAR(500) 
);

CREATE TABLE Galeria_Drinks (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
titulo VARCHAR(500) ,
descricao TEXT ,
path_imagem VARCHAR(500) ,
path_imagem_thumbnail VARCHAR(500)
);


CREATE TABLE Galeria_Figurino (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
titulo VARCHAR(500) ,
descricao TEXT ,
path_imagem VARCHAR(500),
path_imagem_thumbnail VARCHAR(500)
);

CREATE TABLE Midias_Sociais (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
embed VARCHAR(5000) 


);

CREATE TABLE Galeria_Banner(

id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
path_imagem VARCHAR(500),
path_imagem2 VARCHAR(500)


);

INSERT INTO Usuario_Apollo (nome,email,login,senha) values('Geferson','geferson.rd@gmail.com','gefersondk15','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3');


INSERT INTO Home values('1','titulo_banner', 'subtitulo_banner', 'texto_de_apresentacao', 'texto_sobre','texto_missao','texto_fundadores','texto_plano_de_fidelidade','url_youtube_video_explicativo','texto_como_contratar',
'texto_formulario_contato_comum', 'texto_formulario_contato_orcamento','path_imagem_banner','link_instagram','link_youtube','link_facebook');

INSERT INTO Rodape values ('1','texto_rodape1','texto_rodape2','path_imagem','path_imagem_thumbnail');

INSERT INTO Galeria_Banner values ('1','path_imagem','path_imagem2');

INSERT INTO Usuario_Apollo (nome,email,login,senha) values('Paulo','laracruz.paulo@gmail.com','paulo.laracruz!321','edc6e9fd91117fcccdd0612fd64ef7e754b3cf20349b9fc2da693f6ed82544a2');

INSERT INTO Usuario_Apollo (nome,email,login,senha) values('Alisson','narokwq@gmail.com','narok321','edc6e9fd91117fcccdd0612fd64ef7e754b3cf20349b9fc2da693f6ed82544a2');
/*
ALTER TABLE Home ADD texto_missao TEXT;

UPDATE HOME set texto_missao  = "texto_missao";*/