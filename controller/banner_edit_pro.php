<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]))
   {
   	  $mensagem_erro = "<div class='alert alert-danger' role='alert'>Sessão encerrada.</div>";
      //header("Location:login.php");
      //echo $mensagem_erro;
      header("Location:login");
      

   }
   else{

   	include_once 'conexao.php';
	include_once 'functionsGerenciadorConteudoPDO.php';
	
	//header("Content-Type: text/html; charset=utf-8",true);

	
	$titulo = $_POST['titulo'];
	$subtitulo = $_POST['subtitulo'];
	$img_banner = $_FILES['img_banner'];

	$mensagem_sucesso = "<div class='alert alert-success' role='alert'>Banner atualizado com sucesso!</div>";
	$mensagem_erro = "<div class='alert alert-danger' role='alert'>Não foi possível atualizar o banner.</div>";
	

	if(isset($_SESSION["login"])){
		if(isset($titulo) && isset($subtitulo) && $img_banner['size'] > 0){
			try{
				$errors= array();
				$file_name = $img_banner['name'];
				$file_tmp =$img_banner['tmp_name'];
				$file_size =$img_banner['size'];
				$extensions = array("jpeg","jpg","png");
				$file_ext=explode('.',$img_banner['name'])	;
				$file_ext=end($file_ext);  

				if(in_array($file_ext,$extensions ) === false){
						$errors[]="extension not allowed";
				}  

				if($file_size > 5242880){
						$errors[]='File size must be less than 5 MB';
			    }
			    $desired_dir="../media/banner";
			    
			    if(empty($errors)==true){
			    	$name="banner.png";
			    	if(file_exists($desired_dir."/".$name)){
			    		unlink($desired_dir."/".$name);
			    	}
			    	
			    	move_uploaded_file($file_tmp,$desired_dir."/".$name);//$file_name
			    	updateBanner($titulo,$subtitulo,$desired_dir."/".$name);
			    	header("Location:admin");
			    }
			    else{
			    	//echo "Apenas arquivos jpg, png e jpeg podem ser utilizados";
			    	header( "refresh:3;banner_edit" ); 
			    }
			}catch(Exception $e){
				//echo $e;
				header("Location:banner_edit");
			}
			

		}else{
			if(isset($titulo) && isset($subtitulo)){
				//unlink(getImgBRAtual("Home")[0]->path_imagem);
				updateBannerTS($titulo,$subtitulo);
				header("Location:admin");
			}
		}
	}
	else{
		//echo "else";
	}

   }
?>

<?php
	
	

	
	
	
?>