<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (isset($_SESSION["logado"]) && (($_SESSION["logado"])))
   {
      header("Location:admin");
   }
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>apollo login</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="../bootstrap_login/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap_login/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../bootstrap_login/assets/css/form-elements.css">
        <link rel="stylesheet" href="../bootstrap_login/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            body{
                /*background-image: url(../bootstrap_login/assets/img/backgrounds/5.jpg);12181F 4C4C4C*/
                background-color: #12181F;
                
            }
        </style>

    

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Apollo</strong></h1>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Recuperação de senha</h3>
                            		<p>Informe o email de recuperação</p>
                        		</div>
                        		<div class="form-top-right">
                        			<!--<i class="fa fa-lock"></i>-->
                                    <img src="../img/Logo.jpg" alt="" class="img-circle img-polaroid">
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="passwordRecover_pro"class="login-form" id="formRecoverPassword">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Email</label>
			                        	<input type="email" name="email_recover" placeholder="email..." class="form-username form-control" id="form-username" autocomplete="off" required>
			                        </div>
			                        
			                        <button type="submit" class="btn">Entrar</button>
			                    </form>

		                    </div>
                            <div style="padding-top:2em;"><a href="login"><button class="btn pull-right" >Voltar</button></a></div>
                            
                            <div id="result">
                                
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="../bootstrap_login/assets/js/jquery-1.11.1.min.js"></script>
        <script src="../bootstrap_login/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../bootstrap_login/assets/js/jquery.backstretch.min.js"></script>
        <script src="../bootstrap_login/assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

        <script>

            $( "#formRecoverPassword" ).submit(function( event ) {

              // Stop form from submitting normally
              event.preventDefault();
             
              // Get some values from elements on the page:
              var $form = $( this ),
                email_recover = $form.find( "input[name='email_recover']" ).val(),
                
                url = $form.attr( "action" );
                
              // Send the data using post
              var posting = $.post( url, { email_recover: email_recover } );
             
              // Put the results in a div
              posting.done(function( data ) {

                //var content = $( data ).find( "#content" );

                //if(data.localeCompare("session_error2")){//
                    //window.location.reload(); 
                    
                //}else{
                    $( "#result" ).empty().append( data );
                    //$( "#result" ).empty().append( data.localeCompare('session_error') );
                    $("#formRecoverPassword").trigger('reset');


                //}
                function relload(){
                  //window.location.reload();
                  //window.location = "admin";
                }
                //setTimeout(relload, 500);
              });
            });

    </script>

        </script>

    </body>

</html>