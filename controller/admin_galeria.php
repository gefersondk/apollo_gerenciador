<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/style_admin.css">
</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li class="active"><a href="admin_galeria">Galeria</a></li>
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				        <li><a href="#">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				
				<ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#drinks" aria-controls="drinks" role="tab" data-toggle="tab" id="tabDrinks">Drinks</a></li>
                    <li role="presentation"><a href="#service" aria-controls="service" role="tab" data-toggle="tab" id="tabService">Serviço</a></li>
                    <li role="presentation"><a href="#figurino" aria-controls="figurino" role="tab" data-toggle="tab" id="tabfigurino">Figurino</a></li>
                </ul>

    			

				<!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="drinks">
                                <!--inicio conteudo-->
                                <a href="galeria_Upload/upload.html">Nova Imagem</a>
                                <br>
                                 <div class="list-group">
									  <a href="#" class="list-group-item active">First item</a>
									  <a href="#" class="list-group-item">Second item</a>
									  <a href="#" class="list-group-item">Third item</a>
								</div>
                                <!--fim conteudo-->

                    </div>
                    <div role="tabpanel" class="tab-pane" id="service">
                        
                            <!--inicio conteudo-->
                            	<a href="#">Nova Imagem</a>
                                <br>
                                <div class="list-group">
									  <a href="#" class="list-group-item active">First item</a>
									  <a href="#" class="list-group-item">Second item</a>
									  <a href="#" class="list-group-item">Third item</a>
								</div>
                                <!--fim conteudo-->

                    </div>


                    <div role="tabpanel" class="tab-pane" id="figurino">
                    			<a href="#">Nova Imagem</a>
                                <!--inicio conteudo-->
                                <br>
                                <div class="list-group">
									  <a href="#" class="list-group-item active">First item</a>
									  <a href="#" class="list-group-item">Second item</a>
									  <a href="#" class="list-group-item">Third item</a>
								</div>
                                <!--fim conteudo-->

                    </div>

                </div>

            </div><!--fim-->  

			</div>
		</div>
	</div>
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

</body>
</html>