<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (isset($_SESSION["logado"]) && (($_SESSION["logado"])))
   {
      header("Location:admin");
   }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Apollo</title>
    <!-- Bootstrap -->
    <link href="../bootstrap_login/css/bootstrap.css" rel="stylesheet">
    <link href="../bootstrap_login/css/bootstrap-responsive.css" rel="stylesheet">
     <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-image: url("../img/bg.jpg");
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;

        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      #msg{
        margin-top: 7%;
        text-align: center;
      }

      .img_logo{
        display: block;
        margin-left: auto;
        margin-right: auto;
        padding: 0.5em;
        width: 11em;
        box-shadow: 0 0 7px black;
      }


    </style>
  </head>
  <body>

      
      <div class="container">
      
      <form class="form-signin" action="login_autentication" method="POST">
        <img src="../img/Logo.jpg" alt="" class = "img-circle img-polaroid img_logo">
        <!--<h2 class="form-signin-heading">Apollo Admin</h2>-->
        <div style="padding:1em;">
          <input type="text" class="input-block-level" placeholder="Login" name="login">
          <input type="password" class="input-block-level" placeholder="Senha" name="senha">
         
          <button class="btn btn-large btn-primary" type="submit" style="width:100%;">Entrar</button>
        </div>

      </form>

      <div id="msg">
        <?php
          @$v = $_GET['login'];
              if($v == '0'){
              echo "<p style='color:white'>Usuario e/ou senha invalidos</p>";
          }
        ?>
      </div>

    </div>

    
      
 
    
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    
  </body>
</html>

