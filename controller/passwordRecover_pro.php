<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (isset($_SESSION["logado"]) && $_SESSION["logado"] = true)
   {
   	  $mensagem_erro = "<div class='alert alert-danger' role='alert'>Sessão já iniciada.</div>";
      //header("Location:login.php");
      echo "<script>window.location.href='login'</script>";
      

   }
   else{

	   	include_once 'conexao.php';
		include_once 'functionsPDO.php';
		
		header("Content-Type: text/html; charset=utf-8",true);

		
		$email_recover = $_POST['email_recover'];
		
		$mensagem_sucesso = "<div class='alert alert-success' role='alert'>Foi enviado uma nova senha para o seu email!.</div>";
		$mensagem_erro = "<div class='alert alert-danger' role='alert'>O email informado não é válido.</div>";
		
		if(isset($email_recover)){
			if(verificaExistenciaEmail($email_recover)){
					$token = gerarSenha();
					updateRecoverSenha($email_recover, $token);

					// Check for empty fields
					if(!filter_var($email_recover,FILTER_VALIDATE_EMAIL))
					{
						echo "No arguments Provided!";
						//return false;
					}
						
					else{


						// Create the email and send the message
						$to = $email_recover; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
						$email_subject = "Recuperação";
						$email_body = "Você solicitou uma troca de senha, utilize a nova senha ".$token."  para logar no sistema e em seguida recomendamos que você troque em alterar senha.";
						$headers = "From: laracruz.paulo@gmail.com\r\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
						$headers .= "Reply-To: laracruz.paulo@gmail.com\r\n";	
						mail($to,$email_subject,$email_body,$headers);
						//echo $token;
						echo $mensagem_sucesso;
						//laracruz.paulo@gmail.com
					}
			}
			else{
				
				echo $mensagem_erro;
			}
		}
		else{
			echo "else";
		}

   }
?>
