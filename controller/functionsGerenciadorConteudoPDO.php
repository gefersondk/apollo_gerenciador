<?php 
@session_start();
include_once 'conexao.php';
//header("Content-Type: text/html; charset=utf-8",true);


//inicio novas

function updateBanner($titulo, $subtitulo, $path_img){
    $pdo = conectarComPdo();
      

    $updateBanner = $pdo->prepare("UPDATE Home set titulo_banner=:titulo_banner ,subtitulo_banner=:subtitulo_banner, path_imagem=:path_imagem_banner");
    
    $updateBanner->bindValue(":titulo_banner", $titulo);
    $updateBanner->bindValue(":subtitulo_banner", $subtitulo);
    $updateBanner->bindValue(":path_imagem_banner", $path_img);
    

    $updateBanner->execute();
    
    if($updateBanner->rowCount()==1):
        return true;
    else:
        return false;
    endif;
           
    
}

//atualiza apenas titulo e subtitulo
function updateBannerTS($titulo, $subtitulo){
	$pdo = conectarComPdo();
      

    $updateBanner = $pdo->prepare("UPDATE Home set titulo_banner=:titulo_banner ,subtitulo_banner=:subtitulo_banner");
    
    $updateBanner->bindValue(":titulo_banner", $titulo);
    $updateBanner->bindValue(":subtitulo_banner", $subtitulo);
    

    $updateBanner->execute();
    
    if($updateBanner->rowCount()==1):
        return true;
    else:
        return false;
    endif;
}

function getBanner(){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT titulo_banner, subtitulo_banner, path_imagem FROM Home");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao pegar banner>";
    }

}

function getImgBRAtual($table_name){
	$pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT path_imagem FROM ".$table_name."");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao pegar imagem>";
    }
}

function updateRodape($texto1, $texto2, $path_img){
    $pdo = conectarComPdo();
      

    $updateRodape = $pdo->prepare("UPDATE Rodape set texto_rodape1=:texto1 ,texto_rodape2=:texto2, path_imagem=:path_imagem");
    
    $updateRodape->bindValue(":texto1", $texto1);
    $updateRodape->bindValue(":texto2", $texto2);
    $updateRodape->bindValue(":path_imagem", $path_img);
    

    $updateRodape->execute();
    
    if($updateRodape->rowCount()==1):
        return true;
    else:
        return false;
    endif;
               
}

function updateBannerGallery($path_img){
    $pdo = conectarComPdo();
      

    $updateBannerGallery = $pdo->prepare("UPDATE Galeria_Banner set path_imagem=:path_imagem");
    
    $updateBannerGallery->bindValue(":path_imagem", $path_img);
    

    $updateBannerGallery->execute();
    
    if($updateBannerGallery->rowCount()==1):
        return true;
    else:
        return false;
    endif;
               
}

function updateRodapeTS($texto1, $texto2){
	$pdo = conectarComPdo();
      

    $updateRodape = $pdo->prepare("UPDATE Rodape set texto_rodape1=:texto1 ,texto_rodape2=:texto2");
    
    $updateRodape->bindValue(":texto1", $texto1);
    $updateRodape->bindValue(":texto2", $texto2);
    

    $updateRodape->execute();
    
    if($updateRodape->rowCount()==1):
        return true;
    else:
        return false;
    endif;
}


function getRodape(){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT texto_rodape1, texto_rodape2, path_imagem FROM Rodape");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao pegar Rodape>";
    }

}

function getBannerGaleriaM(){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT path_imagem FROM Galeria_Banner");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao pegar Banner Galeria>";
    }

}


function updateAbout($apresentacao,$valores,$missao,$fundadores,$planoDeFidelidade,$comoContratar,$url_youtube_video_explicativo){

    $pdo = conectarComPdo();
      

    $updateAbout = $pdo->prepare("UPDATE Home set texto_de_apresentacao=:texto_de_apresentacao ,texto_valores=:texto_valores, texto_missao=:texto_missao,texto_fundadores=:texto_fundadores, texto_plano_de_fidelidade=:texto_plano_de_fidelidade, 
        texto_como_contratar=:texto_como_contratar, url_youtube_video_explicativo=:url_youtube_video_explicativo");
    
    $updateAbout->bindValue(":texto_de_apresentacao", $apresentacao);
    $updateAbout->bindValue(":texto_valores", $valores);
    $updateAbout->bindValue(":texto_missao", $missao);
    $updateAbout->bindValue(":texto_fundadores", $fundadores);

    $updateAbout->bindValue(":texto_plano_de_fidelidade", $planoDeFidelidade);
    $updateAbout->bindValue(":texto_como_contratar", $comoContratar);
    $updateAbout->bindValue(":url_youtube_video_explicativo", $url_youtube_video_explicativo);
    

    $updateAbout->execute();
    
    if($updateAbout->rowCount()==1):
        return true;
    else:
        return false;
    endif;


}

function getAbout(){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT texto_de_apresentacao, texto_valores, texto_missao,texto_fundadores, texto_plano_de_fidelidade, texto_como_contratar, url_youtube_video_explicativo FROM Home");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar Sobre";
    }

}

function updateForm($formComum, $formOrcamento){
    $pdo = conectarComPdo();
      

    $updateForm = $pdo->prepare("UPDATE Home set texto_formulario_contato_comum=:texto_formulario_contato_comum, texto_formulario_contato_orcamento=:texto_formulario_contato_orcamento");
    
    $updateForm->bindValue(":texto_formulario_contato_comum", $formComum);
    $updateForm->bindValue(":texto_formulario_contato_orcamento", $formOrcamento);
    

    $updateForm->execute();
    
    if($updateForm->rowCount()==1):
        return true;
    else:
        return false;
    endif;
}

function getTextFormContato(){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT texto_formulario_contato_comum, texto_formulario_contato_orcamento FROM Home");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar texto form contato";
    }

}

function addServico($titulo, $descricao, $path_imagem){
    $pdo = conectarComPdo();
        
    try{

        $cadastrar = $pdo->prepare("INSERT INTO Servico (titulo,descricao,path_imagem) VALUES (:titulo, :descricao, :path_imagem)");       
        $cadastrar->bindValue(":titulo", $titulo);
        $cadastrar->bindValue(":descricao", $descricao);
        $cadastrar->bindValue(":path_imagem", $path_imagem);   
        $cadastrar->execute();
                    
        if($cadastrar->rowCount()==1):
            return true;
        else:
            return false;
        endif;

    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>Erro ao cadastrar Serviço na base de dados<br></div>";
    }   
  
}

function updateServico($id, $titulo, $descricao, $path_imagem){
    $pdo = conectarComPdo();
      

    $updateForm = $pdo->prepare("UPDATE Servico set titulo=:titulo, descricao=:descricao, path_imagem=:path_imagem WHERE id=:id");
    
    $updateForm->bindValue(":titulo", $titulo);
    $updateForm->bindValue(":descricao", $descricao);
    $updateForm->bindValue(":path_imagem", $path_imagem);
    $updateForm->bindValue(":id", $id);
    

    $updateForm->execute();
    
    if($updateForm->rowCount()==1):
        return true;
    else:
        return false;
    endif;
}

function updateServicoNotImg($id, $titulo, $descricao){
    $pdo = conectarComPdo();
      

    $updateForm = $pdo->prepare("UPDATE Servico set titulo=:titulo, descricao=:descricao WHERE id=:id");
    
    $updateForm->bindValue(":titulo", $titulo);
    $updateForm->bindValue(":descricao", $descricao);
    $updateForm->bindValue(":id", $id);
    

    $updateForm->execute();
    
    if($updateForm->rowCount()==1):
        return true;
    else:
        return false;
    endif;
}

function getImgServicoAtual($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT path_imagem FROM Servico Where id=:id");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao pegar imagem do serviço selecionado";
    }
}

function getAllServicos(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Servico");//ORDER BY titulo
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os serviços<br>";
    }
}

function delServico($id){
    $pdo = conectarComPdo();
      

    $delServico = $pdo->prepare("delete from Servico where id=:id");
    


    $delServico->bindValue(":id", $id);

    if($dados = getImgServicoAtual($id)){
        $delServico->execute();
        unlink($dados[0]->path_imagem);
        //unlink($dados[0]->path_imagem_thumbnail);
        if($delServico->rowCount()==1):
            return true;
        else:
            return false;
        endif;
    }
    
}

function getServicoId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT titulo, descricao, path_imagem FROM Servico Where id=:id");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao pegar serviço selecionado";
    }
}


function addMidia($codigo){
    $pdo = conectarComPdo();
        
    try{

        $cadastrar = $pdo->prepare("INSERT INTO Midias_Sociais (embed) VALUES (:codigo)");       
        $cadastrar->bindValue(":codigo", $codigo);
        $cadastrar->execute();
                    
        if($cadastrar->rowCount()==1):
            return true;
        else:
            return false;
        endif;

    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>Erro ao cadastrar Mídia na base de dados<br></div>".$e;
    }   
  
}

function getMidia(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Midias_Sociais ORDER BY id DESC");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as Midias.<br>";
    }
}

function getMidiasScroll($limit, $inicio){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Midias_Sociais ORDER BY id DESC LIMIT ".$limit." , ".$inicio." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as midias<br>". $e;
    }

}

function getRowMidias_Sociais(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Midias_Sociais");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas midias<br>". $e;
    }
}


function removerdMidia($id){
    $pdo = conectarComPdo();
        
    try{

        $removerdMidia = $pdo->prepare("delete from Midias_Sociais where id=:id");       
        $removerdMidia->bindValue(":id", $id);
        $removerdMidia->execute();
                    
        if($removerdMidia->rowCount()==1):
            return true;
        else:
            return false;
        endif;

    } catch (PDOException $e) {
        echo "<div class='alert alert-error'>Erro ao cadastrar Mídia na base de dados<br></div>";
    }   
  
}

function UpdateMidiaApollo($youtube, $instagram, $facebook){

    $pdo = conectarComPdo();
      

    $updateForm = $pdo->prepare("UPDATE Home set link_instagram=:instagram, link_youtube=:youtube, link_facebook=:facebook");
    
    $updateForm->bindValue(":instagram", $instagram);
    $updateForm->bindValue(":facebook", $facebook);
    $updateForm->bindValue(":youtube", $youtube);
   
    

    $updateForm->execute();
    
    if($updateForm->rowCount()==1):
        return true;
    else:
        return false;
    endif;
}

function getMidiaApollo(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT link_facebook, link_youtube, link_instagram FROM Home");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as Midias Apollo.<br>";
    }
}

function getIndex(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Home");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
    }catch(PDOException $e){
        echo "Erro ao listar as Midias Apollo.<br>";
    }
}


function getAllImageServicosUlt(){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Servico ORDER BY id DESC");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens servico<br>". $e;
    }

}

function getAllImageServicosScrollWeb($inicio, $limit){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Servico ORDER BY id DESC LIMIT ".$inicio." , ".$limit." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens servico<br>". $e;
    }

}

function getRowServicosScrollWeb(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Galeria_Servico");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();

        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas servico Web<br>". $e;
    }
}

function getAllImageFigurinoScrollWeb($inicio, $limit){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Figurino ORDER BY id DESC LIMIT ".$inicio." , ".$limit." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens figurino<br>". $e;
    }

}

function getRowFigurinoScrollWeb(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Galeria_Figurino");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();

        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas figurino Web<br>". $e;
    }
}


function getAllImageDrinksScrollWeb($inicio, $limit){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Drinks ORDER BY id DESC LIMIT ".$inicio." , ".$limit." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens drinks<br>". $e;
    }

}


function getRowDrinksScrollWeb(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Galeria_Drinks");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();

        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas drinks Web<br>". $e;
    }
}

?>
