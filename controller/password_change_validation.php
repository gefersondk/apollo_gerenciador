<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]))
   {
   	  $mensagem_erro = "<div class='alert alert-danger' role='alert'>Sessão encerrada.</div>";
      //header("Location:login.php");
      echo $mensagem_erro;
      

   }
   else{

   	include_once 'conexao.php';
	include_once 'functionsPDO.php';
	
	header("Content-Type: text/html; charset=utf-8",true);

	
	$senhaAtual = $_POST['currentPassword'];
	$novaSenha = $_POST['newPassword'];
	$confirmarSenha = $_POST['confirmPassword'];

	$mensagem_sucesso = "<div class='alert alert-success' role='alert'>Senha alterada com sucesso!</div>";
	$mensagem_erro = "<div class='alert alert-danger' role='alert'>Não foi possível alterar a senha, informe os dados validos.</div>";
	$mensagem_senha_diferentes = "<div class='alert alert-warning' role='alert'>Não foi possível alterar a senha, as senhas são diferentes.</div>";
	$mensagem_erro_senha_atual_invalida = "<div class='alert alert-warning' role='alert'>Senha atual inválida!..</div>";
	$mensagem_erro_senha_quantidade = "<div class='alert alert-warning' role='alert'>Senhas devem possuir entre 6 - 15 caracteres..</div>";

	if(isset($_SESSION["login"])){
		if(isset($senhaAtual) && isset($novaSenha) && isset($confirmarSenha)){
			if(strcmp($novaSenha,$confirmarSenha) != 0){
				echo $mensagem_senha_diferentes;
			}
			if((strlen($novaSenha) >= 6) && (isset($_SESSION["login"]))){
				if(updateSenha($_SESSION["login"], $senhaAtual, $novaSenha)){
					echo $mensagem_sucesso;

				}else{
					echo $mensagem_erro_senha_atual_invalida;
				}
			}else{
				echo $mensagem_erro_senha_quantidade;
			}

		}else{
			echo $mensagem_erro;
		}
	}
	else{
		echo "else";
	}

   }
?>

<?php
	
	

	
	
	
?>