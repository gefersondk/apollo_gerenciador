<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]))
   {
   	  $mensagem_erro = "<div class='alert alert-danger' role='alert'>Sessão encerrada.</div>";
      //header("Location:login.php");
      echo $mensagem_erro;
      header("Location:login");
      

   }
   else{

   	include_once 'conexao.php';
	include_once 'functionsGerenciadorConteudoPDO.php';
	
	header("Content-Type: text/html; charset=utf-8",true);

	
	$titulo = $_POST['titulo'];
	$descricao = $_POST['descricao'];
	$img_servico = $_FILES['img_servico'];
	$id_servico = $_POST['btn_edit'];

	$mensagem_sucesso = "<div class='alert alert-success' role='alert'>Atualizado com sucesso!</div>";
	$mensagem_erro = "<div class='alert alert-danger' role='alert'>Não foi possível atualizar o servico.</div>";
	

	if(isset($_SESSION["login"])){
		if(isset($titulo) && isset($descricao) && $img_servico['size'] > 0){
			try{
				$errors= array();
				$file_name = $img_servico['name'];
				$file_tmp =$img_servico['tmp_name'];
				$file_size =$img_servico['size'];
				$extensions = array("jpeg","jpg","png");
				$file_ext=explode('.',$img_servico['name'])	;
				$file_ext=end($file_ext);  

				if(in_array($file_ext,$extensions ) === false){
						$errors[]="extension not allowed";
				}  

				if($file_size > 5242880){
						$errors[]='File size must be less than 5 MB';
			    }
			    $desired_dir="../media/servico";
			    if(empty($errors)==true){
			    
			    	foreach (getImgServicoAtual($id_servico) as $key => $value) {
			    		unlink($value->path_imagem);
			    	}
			    	$newName= time()."servico.png";
			    	move_uploaded_file($file_tmp,$desired_dir."/".$newName);
			    	updateServico($id_servico,$titulo,$descricao, $desired_dir."/".$newName);
			    	header("Location:servicos");
			    }
			    else{
			    	echo "Apenas arquivos jpg, png e jpeg podem ser utilizados";
			    	header( "refresh:3;servicos" ); 
			    }
			}catch(Exception $e){
				echo $e;
				header("Location:servicos");
			}
			

		}else{
			if(isset($titulo) && isset($descricao) && isset($id_servico)){
				//unlink(getImgBRAtual("Home")[0]->path_imagem);
				updateServicoNotImg($id_servico, $titulo,$descricao);
				header("Location:servicos");
			}
		}
	}
	else{
		echo "else";
	}

   }
?>

<?php
	
	

	
	
	
?>