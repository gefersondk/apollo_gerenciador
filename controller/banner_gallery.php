<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 	include_once 'functionsPDO.php';
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link href="../lightbox/src/css/lightbox.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/style_admin.css">
	<style>
		.scrol{
			overflow: auto;
			max-height: 40em;
		}

		.backgroun-panel{
			background: #444;
			background: rgba(0, 0, 0, 0.3);
		}
	</style>



</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li class="active"><a href="upload">Galeria</a></li><!--galeria_upload/upload.php-->
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br" target="_blank">Ver Site</a></li>
				      	<li><a href="password_change">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>

	<!-- Page Content -->
    <div class="container">

        <div class="row">

            

            <div id="menu_tabs_galley"><!--inicio-->              
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="upload" class="textColor_gallery">Drinks</a></li>
                    <li role="presentation"><a href="uploadServicos" class="textColor_gallery">Serviço</a></li>
                    <li role="presentation"><a href="uploadFigurinos" class="textColor_gallery">Figurino</a></li>
                    <li role="presentation" class="active"><a href="#" class="textColor_gallery">Banners</a></li>
                </ul>


          
            </div><!--fim-->  

            
        </div>

        <br>


    </div>
    <!-- /.container -->
	
	<div id="result"></div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="panel panel-default">
					  <div class="panel-heading">
					    <h6>Banner Galeria/Mídias Sociais</h6>
					  </div>
					  <div class="panel-body">
					    
							<form id="formUload" action="file-upload-banner-gallery"  enctype="multipart/form-data" method="POST"><!--action="file-upload.php" enctype="multipart/form-data"-->
							    <label for="fileElementId">resolução indicada: (2000x240)</label>
							    <input type="file" name="img_banner_gallery"  id="fileElementId" class="btn btn-success" required/>
							    
							    <input type="submit" value="Enviar"class="button-modify pull-right"/>
							    <!--<input type="button" value="Upload" />-->


							</form>
					
	  				</div>
				</div>

			</div>
		</div>
	</div>

	

	

	
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

	


</body>
</html>