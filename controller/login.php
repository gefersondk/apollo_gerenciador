<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (isset($_SESSION["logado"]) && (($_SESSION["logado"])))
   {
      header("Location:admin");
      
   }
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>apollo login</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="../bootstrap_login/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap_login/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../bootstrap_login/assets/css/form-elements.css">
        <link rel="stylesheet" href="../bootstrap_login/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            body{
                /*background-image: url(../bootstrap_login/assets/img/backgrounds/5.jpg);12181F 4C4C4C*/
                background-color: #12181F;
                
            }
        </style>

        

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Apollo</strong></h1>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Gerenciador</h3>
                            		<p>Entre com login e senha</p>
                        		</div>
                        		<div class="form-top-right">
                        			<!--<i class="fa fa-lock"></i>-->
                                    <img src="../img/Logo.jpg" alt="" class="img-circle img-polaroid">
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="login_autentication"  class="login-form" id="formLoginPassword">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Login</label>
			                        	<input type="text" name="login" placeholder="Login..." class="form-username form-control" id="form-username" autocomplete="off" required>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Senha</label>
			                        	<input type="password" name="password" placeholder="Senha..." class="form-password form-control" id="form-password" autocomplete="off" required>
			                        </div>
			                        <button type="submit" class="btn">Entrar</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<a href="passwordRecover"><h5 style="color:#ffffff;">Recuperar Senha</h5></a>
                        	
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                            <div id="msg">
                                <div id="result"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="../bootstrap_login/assets/js/jquery-1.11.1.min.js"></script>
        <script src="../bootstrap_login/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../bootstrap_login/assets/js/jquery.backstretch.min.js"></script>
        <script src="../bootstrap_login/assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

        <script>
            $( "#formLoginPassword" ).submit(function( event ) {

              // Stop form from submitting normally
              event.preventDefault();
             
              // Get some values from elements on the page:
              var $form = $( this ),
                term_login = $form.find( "input[name='login']" ).val(),
                term_password = $form.find( "input[name='password']" ).val(),
                
                url = $form.attr( "action" );
                
              // Send the data using post
              var posting = $.post( url, { term_login: term_login, term_password: term_password } );
             
              // Put the results in a div
              posting.done(function( data ) {

                //var content = $( data ).find( "#content" );

                //if(data.localeCompare("session_error2")){//
                    //window.location.reload(); 
                    
                //}else{
                    $( "#result" ).empty().append( data );
                    //$( "#result" ).empty().append( data.localeCompare('session_error') );
                    $("#formLoginPassword").trigger('reset');


                //}
                function relload(){
                  //window.location.reload();
                  window.location = "admin";
                }
                //setTimeout(relload, 500);
              });
            });

        </script>

        <script>

            $( "#form-username" ).focus(function() {
              $( "#result" ).empty();
            });

            $( "#form-password" ).focus(function() {
             $( "#result" ).empty();
            });

        </script>

    </body>

</html>