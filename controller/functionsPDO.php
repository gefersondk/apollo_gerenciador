<?php 
@session_start();
include_once 'conexao.php';
//header("Content-Type: text/html; charset=utf-8",true);


function cadastrarUsuario($dados = array()){
	$pdo = conectarComPdo();
        if(verificaExistencia($dados['login'])){
            echo "<div class='alert alert-error'>Usuario com login ja cadastrado</div>";
            return 0;
        }
        if(is_array($dados)):
            try{
                $cadastrar = $pdo->prepare("INSERT INTO Usuario_Apollo (login,nome,senha,tipo) VALUES (:login, :nome, :senha , :tipo)");
                
                foreach($dados as $i=>$j):
                    $cadastrar->bindValue(":$i", $j);
                endforeach;
         
                $cadastrar->execute();
                
                if($cadastrar->rowCount()==1):
                    return true;
                else:
                    return false;
                endif;
            } catch (PDOException $e) {
                echo "<div class='alert alert-error'>Erro ao cadastrar Usuario na base de dados<br></div>";
            }   
        endif;
}

function criptografar($senha){
    //return hash('md5', $senha); 
    return hash('sha256', $senha);
}

function verificarUsuario($login,$senha){
    $nSenha = criptografar($senha);
    if(verificaExistencia($login)){
        $dadosUsuario = getusuarioPorLogin($login);
        if($dadosUsuario[0]->senha == $nSenha){
            //$_SESSION['nome']= $dadosUsuario[0]->nome;
        return true;
        }
    }
        
    return false;

}

function getIdUsuario($login,$senha){
    $nSenha = criptografar($senha);
    if(verificaExistencia($login)){
        $dadosUsuario = getusuarioPorLogin($login);
        if($dadosUsuario[0]->senha == $nSenha){
            //$_SESSION['nome']= $dadosUsuario[0]->nome;
            return $dadosUsuario[0]->id;
        }
    }
        
    //return false;

}

//recupera o tipo e o id do usuario
function verificarTipo($login){
    $pdo = conectarComPdo();
    try{
        $getTipo = $pdo->prepare("SELECT id, tipo FROM Usuario_Apollo WHERE login LIKE :login ");
        $getTipo->bindValue(":login",$login);//."%"
        $getTipo->execute();
        
        if($getTipo->rowCount()>0):
            return $getTipo->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}


function getusuarioPorLogin($login){
    $pdo = conectarComPdo();
    try{
        $getUser = $pdo->prepare("SELECT id,login,nome,senha FROM Usuario_Apollo WHERE login LIKE :login ");
        $getUser->bindValue(":login",$login);//."%"
        $getUser->execute();
        
        if($getUser->rowCount()>0):
            return $getUser->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}

function getUsuarioPorId($id){
    $pdo = conectarComPdo();
    try{
        $getUsuarioPorId = $pdo->prepare("SELECT nome FROM Usuario_Apollo WHERE id = :id ");
        $getUsuarioPorId->bindValue(":id",$id);//."%"
        $getUsuarioPorId->execute();
        
        if($getUsuarioPorId->rowCount()>0):
            return $getUsuarioPorId->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}

function UpdateUsuario($nome,$senha){
    $pdo = conectarComPdo();
    $novaSenha = criptografar($senha);
    try{
        $update = $pdo->prepare("UPDATE Usuario_Apollo SET nome = :nome, senha = :senha WHERE id = :id; ");
        $update->bindValue(":nome",$id);//."%"
        $update->bindValue(":senha",$novaSenha);
        $update->execute();
        
        if($update->rowCount()>0):
            return $update->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao selecionar o usuario<br>";
    }
}

function updateSenha($login,$senhaAtual, $nova_Senha){
    $pdo = conectarComPdo();

    if(verificarUsuario($login, $senhaAtual)){
        $novaSenha = criptografar($nova_Senha);
        $id = getIdUsuario($login, $senhaAtual);//TODO
        try{
            $update = $pdo->prepare("UPDATE Usuario_Apollo SET  senha = :senha WHERE id = :id; ");
            $update->bindValue(":id",$id);//."%"
            $update->bindValue(":senha",$novaSenha);
            $update->execute();
            
            //if($update->rowCount()>0):
                //return $update->fetchAll(PDO::FETCH_OBJ);
                //return true;
            
            //endif;
            return true;
            
        } catch (PDOException $e) {
            //echo "Erro ao alterar senha do usuario<br>";
            return false;
        }
    }else{
        //echo "Erro em alterar a senha!."
        return false;
    }

    
}

function updateRecoverSenha($email, $nova_Senha){
    $pdo = conectarComPdo();

    $novaSenha = criptografar($nova_Senha);
    
        try{
            $update = $pdo->prepare("UPDATE Usuario_Apollo SET  senha = :senha WHERE email = :email; ");
            $update->bindValue(":email",$email);//."%"
            $update->bindValue(":senha",$novaSenha);
            $update->execute();
            
            //if($update->rowCount()>0):
                //return $update->fetchAll(PDO::FETCH_OBJ);
                //return true;
            
            //endif;
            return true;
            
        } catch (PDOException $e) {
            //echo "Erro ao alterar senha do usuario<br>";
            return false;
        }

    
}


function gerarSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
{
    $lmin = 'abcdefghijklmnopqrstuvwxyz';
    $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $num = '1234567890';
    $simb = '!@#$%*-';
    $retorno = '';
    $caracteres = '';
    $caracteres .= $lmin;
    if ($maiusculas) $caracteres .= $lmai;
    if ($numeros) $caracteres .= $num;
    if ($simbolos) $caracteres .= $simb;
    $len = strlen($caracteres);
    for ($n = 1; $n <= $tamanho; $n++) {
        $rand = mt_rand(1, $len);
        $retorno .= $caracteres[$rand-1];
    }
    return $retorno;
}

function verificaExistencia($login){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT login FROM Usuario_Apollo WHERE login LIKE :login ");
        $listar->bindValue(":login",$login);//."%"
        $listar->execute();

        if($listar->rowCount()>0):
            return true;
        
        endif;
        return false;
        
    } catch (PDOException $e) {
        echo "Erro ao verificar a existencia do usuario<br>";
    }
}

function verificaExistenciaEmail($email){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT email FROM Usuario_Apollo WHERE email LIKE :email ");
        $listar->bindValue(":email",$email);//."%"
        $listar->execute();

        if($listar->rowCount()>0):
            return true;
        
        endif;
        return false;
        
    } catch (PDOException $e) {
        echo "Erro ao verificar a existencia do email<br>";
    }
}

function listarDocumentosComPdo(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Documentos ORDER BY nome");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os filmes<br>";
    }
}

function listarUsuarios(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Usuario_Apollo ORDER BY nome");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os Usuarios<br>";
    }
}






function listarDocumentosPorTitulo($titulo){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT * FROM Aluno a  join DocumentoAcademico da on a.id = da.aluno_id join DocumentoPessoal dp on dp.aluno_id = a.id WHERE a.nome LIKE  :titulo ");
        $listar->bindValue(":titulo",$titulo.'%');
        $listar->execute();
        
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        
        endif;
        
    } catch (PDOException $e) {
        print_r($e);
        echo "Erro ao listar os documentos<br>";
    }

}

function listarDocumentosSimples($nome ='%',$matricula ='%',$rg ='%',$cpf ='%'){

    $pdo = conectarComPdo();
    try{
       
        $listar = $pdo->prepare("SELECT distinct d.id,m.nome,m.tipodocumento,u.login,d.dataleitura,count(img.doc_id) from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Aluno a on m.aluno_id=a.id join 
        Curso c on m.curso_id=c.id join Instituicao i on m.instituicao_id = i.id join Imagem img on d.id=img.doc_id join Usuario u on u.id = d.user_id where a.nome like :nome and a.matricula like :matricula and a.rg like :rg and
        a.cpf like :cpf group by d.id,m.id,img.doc_id,u.id");
        $listar->bindValue(":nome",$nome.'%');
        $listar->bindValue(":matricula",$matricula.'%');
        $listar->bindValue(":rg",$rg.'%');
        $listar->bindValue(":cpf",$cpf.'%');
        $listar->execute();

        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os NOVOS DADOS<br>";
    }

}

function listarDocumentosMetadados($id){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT distinct i.nome, a.nome, m.tipodocumento, a.datanascimento, a.matricula, a.cpf,
        a.rg, a.mae, a.pai, c.nome, c.nivel, c.anoinicio, c.anofim, c.situacao, m.tipodocumento, m.nome, m.anodocumento   
        from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Aluno a on m.aluno_id=a.id join 
        Curso c on m.curso_id=c.id join Instituicao i on m.instituicao_id = i.id where d.id = :id
        group by m.id,c.id,a.id,i.id");
        $listar->bindValue(":id",$id);
        $listar->execute();

        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os NOVOS DADOS<br>";
    }

}

function listarDados($nome="",$matricula="",$rg="",$cpf="",$mae="",$pai=""){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("Select distinct d.id,a.nome as aluno, u.nome as user, d.dataleitura, dp.titulo, count(i.docpes_id) as qtd from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Usuario u on u.id = d.user_id join Aluno a on a.id = m.aluno_id join DocumentoPessoal dp on dp.aluno_id = a.id join Imagem i on i.docpes_id = dp.id 
WHERE (upper(a.nome) LIKE upper(:nome)) and (upper(a.matricula) LIKE upper(:matricula)) and (upper(a.rg) LIKE upper(:rg)) and (upper(a.cpf) LIKE upper(:cpf)) and (upper(a.mae) like upper(:mae)) and (upper(a.pai) like upper(:pai))
GROUP BY d.id,a.nome, u.nome, d.dataleitura, dp.titulo, i.docpes_id
UNION
Select distinct d.id,a.nome as aluno, u.nome as user, d.dataleitura, dp.titulo, count(i.docaca_id) as qtd from DocumentoDigital d join Metadados m on d.metadados_id = m.id join Usuario u on u.id = d.user_id join Aluno a on a.id = m.aluno_id join DocumentoAcademico dp on dp.aluno_id = a.id join Imagem i on i.docAca_id = dp.id
WHERE (upper(a.nome) LIKE upper(:nome)) and (upper(a.matricula) LIKE upper(:matricula)) and (upper(a.rg) LIKE upper(:rg)) and (upper(a.cpf) LIKE upper(:cpf)) and (upper(a.mae) like upper(:mae)) and (upper(a.pai) like upper(:pai))
GROUP BY d.id,a.nome, u.nome, d.dataleitura, dp.titulo, i.docaca_id;");
        
        $listar->bindValue(":nome","%".$nome."%");
        $listar->bindValue(":matricula","%".$matricula."%");
        $listar->bindValue(":rg","%".$rg."%");
        $listar->bindValue(":cpf","%".$cpf."%");
        $listar->bindValue(":mae","%".$mae."%");
        $listar->bindValue(":pai","%".$pai."%");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }

}


function documentoPorId($id){

    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT m.tipoDocumento, m.nome as metadadosNome, m.anoDocumento, i.nome as instituicaoNome,a.nome as alunoNome, a.dataNascimento, a.matricula, a.cpf, a.rg, a.mae, a.pai, c.nome as cursoNome, c.nivel, c.anoinicio, c.anofim, c.situacao from documentoDigital d join Metadados m on d.metadados_id = m.id join Instituicao i on m.instituicao_id = i.id join aluno a on m.aluno_id =a.id join curso c on a.id = c.aluno_id where d.id = :id");
        $listar->bindValue(':id', $id, PDO::PARAM_INT);
       $listar->execute();
         if($listar->rowCount()>0):
                
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
   
}



function imagens($titulo="",$aluno=""){
    $pdo = conectarComPdo();
    try{
        
        $listar = $pdo->prepare("SELECT i.imagem FROM Aluno a join DocumentoPessoal dp on a.id = dp.aluno_id join Imagem i on i.docpes_id = dp.id where upper(dp.titulo) like upper(:titulo) and upper(a.nome) like upper(:aluno) UNION Select  i.imagem from Aluno a join DocumentoAcademico dp on a.id = dp.aluno_id join Imagem i on i.docpes_id = dp.id where upper(dp.titulo) like upper(:tituloo) and upper(a.nome) like upper(:alunoo)");
   
        $listar->bindValue(":titulo",$titulo."%");
        $listar->bindValue(":aluno",$aluno."%");
        $listar->bindValue(":tituloo",$titulo."%");
        $listar->bindValue(":alunoo",$aluno."%");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}

function imagemPessoalAluno($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select i.imagem from Imagem i join DocumentoAcademico da on da.id = i.docaca_id join Aluno a on a.id = da.aluno_id join Metadados met on met.aluno_id = a.id join DocumentoDigital dd on dd.metadados_id = met.id where dd.id = :idDocDig
UNION
select i.imagem from Imagem i join DocumentoPessoal da on da.id = i.docpes_id join Aluno a on a.id = da.aluno_id join Metadados met on met.aluno_id = a.id join DocumentoDigital dd on dd.metadados_id = met.id where dd.id = :idDocDig");
        $listar->bindValue(":idDocDig",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
            
    } catch (PDOException $e) {
            echo "Erro ao listar as Imagens<br>";
    }
}

function teste(){
    $pdo=conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM  Imagem");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);
        
        //assthru(pg_fetch_array($lob));

       
    
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}


//inicio novas

function insertImageDrinks($path_img, $path_img_thumb){
    $pdo = conectarComPdo();
      

    $cadastrar = $pdo->prepare("INSERT INTO Galeria_Drinks (path_imagem,path_imagem_thumbnail) VALUES (:path_img, :path_img_thumb)");
    

    $cadastrar->bindValue(":path_img", $path_img);
    $cadastrar->bindValue(":path_img_thumb", $path_img_thumb);

    $cadastrar->execute();
    
    if($cadastrar->rowCount()==1):
        return true;
    else:
        return false;
    endif;
           
    
}
function insertImageFigurinos($path_img, $path_img_thumb){
    $pdo = conectarComPdo();
      

    $cadastrar = $pdo->prepare("INSERT INTO Galeria_Figurino (path_imagem,path_imagem_thumbnail) VALUES (:path_img, :path_img_thumb)");
    

    $cadastrar->bindValue(":path_img", $path_img);
    $cadastrar->bindValue(":path_img_thumb", $path_img_thumb);

    $cadastrar->execute();
    
    if($cadastrar->rowCount()==1):
        return true;
    else:
        return false;
    endif;
           
    
}

function insertImageServicos($path_img, $path_img_thumb){
    $pdo = conectarComPdo();
      

    $cadastrar = $pdo->prepare("INSERT INTO Galeria_Servico (path_imagem,path_imagem_thumbnail) VALUES (:path_img, :path_img_thumb)");
    

    $cadastrar->bindValue(":path_img", $path_img);
    $cadastrar->bindValue(":path_img_thumb", $path_img_thumb);

    $cadastrar->execute();
    
    if($cadastrar->rowCount()==1):
        return true;
    else:
        return false;
    endif;
           
    
}

function getAllImageDrinks(){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Drinks");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens<br>";
    }

}

function getAllImageDrinksScroll($inicio, $limit){

    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Drinks ORDER BY id DESC LIMIT ".$inicio." , ".$limit." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens drinks<br>". $e;
    }

}

function getAllImageFigurinosScroll($inicio, $limit){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Figurino ORDER BY id DESC LIMIT ".$inicio." , ".$limit." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens figurino<br>". $e;
    }

}

function getAllImageServicosScroll($inicio, $limit){


    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM Galeria_Servico ORDER BY id DESC LIMIT ".$inicio." , ".$limit." ");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens servico<br>". $e;
    }

}



function getRowDrinks(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Galeria_Drinks");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas drinks<br>". $e;
    }
}

function getRowFigurinos(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Galeria_Figurino");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas figurino<br>". $e;
    }
}

function getRowServicos(){
    
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT COUNT(*) FROM Galeria_Servico");//ORDER BY id DESC
        //$listar->bindValue(":mlimit", $limit);
       // $listar->bindValue(":mofsset", $inicio);
        $listar->execute();
        if($listar->rowCount()>0):
            //return $listar->fetchAll(PDO::FETCH_OBJ);
            return $number_of_rows = $listar->fetchColumn(); 
            
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar quantidade de linhas serviços<br>". $e;
    }
}

function delImageGallery($id){
    $pdo = conectarComPdo();
      

    $cadastrar = $pdo->prepare("delete from Galeria_Drinks where id=:id");
    

    //$cadastrar->bindValue(":table", $table);
    $cadastrar->bindValue(":id", $id);

    if($dados = getImageForId($id)){
        $cadastrar->execute();
        unlink($dados[0]->path_imagem);
        unlink($dados[0]->path_imagem_thumbnail);
        if($cadastrar->rowCount()==1):
            return true;
        else:
            return false;
        endif;
    }

    
}

function delImageServicos($id){
    $pdo = conectarComPdo();
      

    $cadastrar = $pdo->prepare("delete from Galeria_Servico where id=:id");
    

    //$cadastrar->bindValue(":table", $table);
    $cadastrar->bindValue(":id", $id);

    if($dados = getImageForServicosId($id)){
        $cadastrar->execute();
        unlink($dados[0]->path_imagem);
        unlink($dados[0]->path_imagem_thumbnail);
        if($cadastrar->rowCount()==1):
            return true;
        else:
            return false;
        endif;
    }

    
}

function delImageFigurinos($id){
    $pdo = conectarComPdo();
      

    $cadastrar = $pdo->prepare("delete from Galeria_Figurino where id=:id");
    

    //$cadastrar->bindValue(":table", $table);
    $cadastrar->bindValue(":id", $id);

    if($dados = getImageForFigurinosId($id)){
        $cadastrar->execute();
        unlink($dados[0]->path_imagem);
        unlink($dados[0]->path_imagem_thumbnail);
        if($cadastrar->rowCount()==1):
            return true;
        else:
            return false;
        endif;
    }

    
}

function getImageForId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT path_imagem, path_imagem_thumbnail FROM Galeria_Drinks WHERE id=:id");
        $listar->bindValue(":id", $id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens<br>";
    }
}

function getImageForServicosId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT path_imagem, path_imagem_thumbnail FROM Galeria_Servico WHERE id=:id");
        $listar->bindValue(":id", $id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens<br>";
    }
}

function getImageForFigurinosId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT path_imagem, path_imagem_thumbnail FROM Galeria_Figurino WHERE id=:id");
        $listar->bindValue(":id", $id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
        
    } catch (PDOException $e) {
        echo "Erro ao listar as imagens<br>";
    }
}

//fim novas

//$v = teste();
//header('Content-Type: image/png');
//foreach ($v as $key) {
    //print_r($key);
   // print_r($key['imagem']);
    //echo "<pre>"; var_dump($key['imagem']); echo "</pre>";
    //echo  stream_get_contents($key['imagem']);

//}

//fpassthru($v[0]->imagem);
//print_r($t);
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";


//$l=base64_encode((string)stream_get_contents($v[0]->imagem));
//print_r($l);

//echo "<img src='data:image/png;base64,".$l."' />";


//echo "oi<img src='C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg'>";
//$a = imagens("D","Gefe");
//foreach ($a as $key) {
    //echo "<img src=".".".$key->imagem.".png"."."."></img>";
//}
?>
