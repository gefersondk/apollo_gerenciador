<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/style_admin.css">
	<script src="../ckeditor/ckeditor.js"></script>


	<style>

		.title_banner{
			color: #6c6360;
			font-size: 2em;
		}

	</style>

</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li><a href="upload">Galeria</a></li>
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br">Ver Site</a></li>
				      	<li><a href="password_change">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12"><a href="admin" class="pull-right" style="padding:11px;">Voltar</a></div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title">Serviços</h3>
					  </div>
					  <div class="panel-body">
					    
							<table class="table table-striped">
							    
							    <tbody>
							      <tr>
							       
							        <td>
							        	<h5 class="title_banner">Eventos Sociais:</h5>
										<form method="Post">

								            <textarea name="editor1" id="editor1" rows="10" cols="80">
								                This is my textarea to be replaced with CKEditor.
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor1' );
								            </script>
								            <label for="link_img_s1">Imagem do Serviço</label>
											  <input type="file"  id="link_img_s1" class="form-control">
											<br>
								            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
								        </form>

							        </td>
							
							      </tr>
							      
							     <tr>
							       
							        <td>
							        	<h5 class="title_banner">Eventos Corporativos:</h5>
										<form method="Post">
								            <textarea name="editor2" id="editor2" rows="10" cols="80">
								                This is my textarea to be replaced with CKEditor.
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor2' );
								            </script>
								            <label for="link_img_s2">Imagem do Serviço</label>
											  <input type="file"  id="link_img_s2" class="form-control">
											<br>
								            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
								        </form>

							        </td>
							
							      </tr>

							      <tr>
							       
							        <td>
							        	<h5 class="title_banner">Coquetéis:</h5>
										<form method="Post">
								            <textarea name="editor3" id="editor3" rows="10" cols="80">
								                This is my textarea to be replaced with CKEditor.
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor3' );
								            </script>
								            <label for="link_img_s3">Imagem do Serviço</label>
											  <input type="file"  id="link_img_s3" class="form-control">
											<br>
								            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
								        </form>

							        </td>
							
							      </tr>

							    </tbody>
							</table>

	  				</div>
				</div>

			</div>
		</div>
	</div>
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

</body>
</html>