<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }
   include_once "functionsGerenciadorConteudoPDO.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/style_admin.css">
	<script src="../ckeditor/ckeditor.js"></script>


	<style>

		.title_banner{
			color: #6c6360;
			font-size: 1em;
			padding-left: 1em;
		}

	</style>

</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li><a href="upload">Galeria</a></li>
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br" target="_blank">Ver Site</a></li>
				      	<li><a href="password_change">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12"><a href="admin" class="pull-right" style="padding:11px;"><button class="button-back">Voltar</button></a></div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="result"></div>
				<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title">Contato</h3>
					  </div>
					  <div class="panel-body">
					    
							<table class="table table-striped">
							    
							    <tbody>
							      <tr>
							       
							        <td>
							        	<?php
							        		$dados = getTextFormContato();
							        	?>
										<form role="form" id="formUpdateContato" action="contato_edit_pro">
										  <div class="form-group">
										    <label  for="editor1" class="title_banner">Texto formulario de contato comum</label>
										    <textarea name="formComum" id="editor1" rows="10" cols="80">
								                <?php echo $dados[0]->texto_formulario_contato_comum ?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor1' );
								            </script>
										  </div>
										  <div class="form-group">
										    <label  for="forOrcamento" class="title_banner">Texto formulario de contato orçamento</label>
										    <textarea name="formOrcamento" id="editor2" rows="10" cols="80">
								                <?php echo $dados[0]->texto_formulario_contato_orcamento ?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor2' );
								            </script>
										  </div>
										  
										  <button type="submit" class="button-modify pull-right">Salvar</button>
										</form>

							        </td>
							
							      </tr>
						
							    </tbody>

							</table>

	  				</div>
				</div>

			</div>
		</div>
	</div>
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <script>
		

		$( "#formUpdateContato" ).submit(function( event ) {

		  // Stop form from submitting normally
		  event.preventDefault();
		 
		  for ( instance in CKEDITOR.instances )
        	CKEDITOR.instances[instance].updateElement();

		  // Get some values from elements on the page:
		  var $form = $( this ),
		    formComum = $form.find( "textarea[name='formComum']" ).val(),
		    formOrcamento = $form.find( "textarea[name='formOrcamento']" ).val(),
		    
		    url = $form.attr( "action" );
		
		  // Send the data using post
		  var posting = $.post( url, { formComum: formComum, formOrcamento: formOrcamento } );
		 
		  // Put the results in a div
		  posting.done(function( data ) {

		    //var content = $( data ).find( "#content" );

		    //if(data.localeCompare("session_error2")){//
		    	//window.location.reload(); 
		    	
		    //}else{
		    	$( "#result" ).empty().append( data );
		    	//$( "#result" ).empty().append( data.localeCompare('session_error') );
		    	//$("#formUpdatePassword").trigger('reset');


		    //}
		    function relload(){
			  window.location = "admin";
			}
			setTimeout(relload, 500);
		  });
		});

    </script>

</body>
</html>