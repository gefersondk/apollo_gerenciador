<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mudar Senha</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../css/style_admin.css">

    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	

</head>
<body>
	

	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li><a href="upload">Galeria</a></li>
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br" target="_blank">Ver Site</a></li>
				      	<li class="active"><a href="#">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>


	<div class="container">
		<h3 class="text_change_password">Alterar senha</h3>
	
		<div class="alert alert-warning" role="alert">Por favor, informe sua senha antiga, por segurança, e então informe sua nova senha duas vezes para que possamos verificar se você digitou corretamente.</div>
		<br>
		<div id="result"></div>
		<div class="row">
			<div class="col-md-4">
				 <form role="form" id="formUpdatePassword" action="password_change_validation">
				  <div class="form-group">
				    <label for="pwd" class="text_change_password">Senha Atual</label>
				    <input type="password" class="form-control" name="currentPassword" required pattern=".{3,15}"   required title="minimo 6 caracteres, maximo 15" autocomplete="off">
				  </div>
				  <div class="form-group">
				    <label for="pwd" class="text_change_password">Nova Senha</label>
				    <input type="password" class="form-control" name="newPassword" required pattern=".{3,15}" id="newPassword" required title="minimo 6 caracteres, maximo 15" autocomplete="off">
				  </div>
				  <div class="form-group">
				    <label for="pwd" class="text_change_password">Confirmar Senha</label>
				    <input type="password" class="form-control" name="confirmPassword" required pattern=".{3,15}" id="confirmPassword" required title="minimo 6 caracteres, maximo 15" autocomplete="off">
				  </div>
				  
				  <button type="submit" class="button-modify  pull-right" id="smt">Salvar</button>
				</form>
			</div>
		</div>
	</div>

	<script>
		/*$(document).ready(function(){
		var form=$("#formUpdatePassword");
		$("#smt").click(function(){
		$.ajax({
		        type:"POST",
		        url:"password_change_validation.php",
		        data:form.serialize(),

		        success: function(response){
		        if(response === 1){
		            document.getElementById("#txtHint").innerHTML = response+"geferson";
		        }  else {
		            document.getElementById("#txtHint").innerHTML = "erro";
		        }
		        }
		    });
		});
		});*/


		$( "#formUpdatePassword" ).submit(function( event ) {

		  // Stop form from submitting normally
		  event.preventDefault();
		 
		  // Get some values from elements on the page:
		  var $form = $( this ),
		    term_currentPassword = $form.find( "input[name='currentPassword']" ).val(),
		    term_newPassword = $form.find( "input[name='newPassword']" ).val(),
		    term_confirmPassword = $form.find( "input[name='confirmPassword']" ).val(),
		    url = $form.attr( "action" );
		 	
		  // Send the data using post
		  var posting = $.post( url, { currentPassword: term_currentPassword, newPassword: term_newPassword, confirmPassword: term_confirmPassword } );
		 
		  // Put the results in a div
		  posting.done(function( data ) {

		    //var content = $( data ).find( "#content" );

		    //if(data.localeCompare("session_error2")){//
		    	//window.location.reload(); 
		    	
		    //}else{
		    	$( "#result" ).empty().append( data );
		    	//$( "#result" ).empty().append( data.localeCompare('session_error') );
		    	$("#formUpdatePassword").trigger('reset');


		    //}
		    function relload(){
			  //window.location.reload();
			  window.location = "admin";
			}
			//setTimeout(relload, 500);
		  });
		});

    </script>


    <script type="text/javascript">

    	var password = document.getElementById("newPassword")
		, confirm_password = document.getElementById("confirmPassword");

		function validatePassword(){
		  if(password.value != confirm_password.value) {
		    confirm_password.setCustomValidity("Senhas diferentes");
		  } else {
		    confirm_password.setCustomValidity('');
		  }
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;

    </script>

</body>
</html>