<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 	include_once 'functionsPDO.php';
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link href="../lightbox/src/css/lightbox.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/style_admin.css">
	<style>
		.scrol{
			overflow: auto;
			max-height: 40em;
		}

		.backgroun-panel{
			background: #444;
			background: rgba(0, 0, 0, 0.3);
		}
	</style>



</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li class="active"><a href="upload">Galeria</a></li><!--galeria_upload/upload.php-->
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br" target="_blank">Ver Site</a></li>
				      	<li><a href="password_change">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>

	<!-- Page Content -->
    <div class="container">

        <div class="row">

            

            <div id="menu_tabs_galley"><!--inicio-->              
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="upload" >Drinks</a></li>
                    <li role="presentation"><a href="uploadServicos" class="textColor_gallery">Serviço</a></li>
                    <li role="presentation"><a href="uploadFigurinos" class="textColor_gallery">Figurino</a></li>
                    <li role="presentation"><a href="banner_gallery" class="textColor_gallery">Banners</a></li>
                </ul>


          
            </div><!--fim-->  

            
        </div>

        <br>


    </div>
    <!-- /.container -->
	
	<div id="result"></div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="panel panel-default">
					  <div class="panel-heading">
					    
					  </div>
					  <div class="panel-body">
					    
							<form id="formUload" action="file-upload"  enctype="multipart/form-data" method="POST"><!--action="file-upload.php" enctype="multipart/form-data"-->
							    <input type="file" name="files[]" multiple="" id="fileElementId" class="btn btn-success" required/>
							    <input type="submit" value="Enviar"class="button-modify pull-right"/>
							    <!--<input type="button" value="Upload" />-->


							</form>
					
	  				</div>
				</div>

			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<!--<div class="scrol scroll3">-->
					<form role="form" id="formDelImg" action="del_image">
						<ul class="list-group" id="dadosHandler">
						<?php
						
							//$path = "media/drinks/thumbnail/";
							//$diretorio = dir($path);
							//echo "Lista de Arquivos do diretório '<strong>".$path."</strong>':<br />";

							//while($arquivo = $diretorio -> read()){
								 //echo "<li class='list-group-item' id='13'><img src='".$path.$arquivo."' style='width:7em;'/><button type='submit' class='btn btn-primary pull-right' id='id_img' value='12'>apagar</button></li>";
							//}
							//$diretorio -> close();
							$dados = getAllImageDrinksScroll(0,10);//getAllImageDrinks();

							if(isset($dados)){
								foreach ($dados as $key => $value) {
									echo "<li class='list-group-item'><a href='".$value->path_imagem."' data-lightbox='roadtrip'><img src='".$value->path_imagem_thumbnail."' style='width:7em;'/></a><button type='submit' class='btn btn-danger pull-right id_img' id='id_img' value='".$value->id."' >apagar</button></li>";
							
								}
							}
						?>
						
						</ul>
					</form>
				<!--</div>-->
				<div id="loadmoreajaxloader" style="display:none;"><center><img src="../img/spinner4.gif" style="width:5em;" /></center></div>
				 
				
			</div>
		</div>
	</div>
	<div id="result3"></div>


	

	
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

	<script>
		
		$( "#formDelImg" ).submit(function( event ) {

		  // Stop form from submitting normally
		  event.preventDefault();
		 
		  // Get some values from elements on the page:
		  var $form = $( this ),
		    //term_currentPassword = $form.find( "input[name='currentPassword']" ).val(),
		    //term_newPassword = $form.find( "input[name='newPassword']" ).val(),
		   
		    //id_img = $(".id_img").val();
			id_img = jQuery("#formDelImg").context.activeElement.value
		    //alert(id_img);
		    url = $form.attr( "action" );
		 	
		  // Send the data using post
		  var posting = $.post( url, { id_img:id_img} );
		 
		  // Put the results in a div
		  posting.done(function( data ) {
		  	//( "#result" ).empty().append( data );
		    //var content = $( data ).find( "#content" );
		    $( "#result" ).empty().append( data );
		    

		  });
		});


	</script>


		<!--<script>

$(document).ready(function(){
    $(".scrol").scroll(function(){
        alert("teste");
    });
});
</script>-->

<!--
   <script>

    	$( "#formUload" ).submit(function( event ) {
    		//alert("chamoy");
		  // Stop form from submitting normally
		  event.preventDefault();
		 
		  // Get some values from elements on the page:
		  var $form = $( this ),
		    files = $form.find( "input[name='files[]']" ).val(),
		    //files = $("#fileElementId").files;
		    url = $form.attr( "action" );
		 	
		  // Send the data using post
		  var posting = $.post( url, { "files": files } );
		 
		  // Put the results in a div
		  posting.done(function( data ) {

		    //var content = $( data ).find( "#content" );
		   	$( "#result" ).empty().append( data );
		    
		  });
		});

    </script>-->

<!--
    <script>

    	$(':button').click(function(){
    		
    var formData = new FormData($('form')[0]);
    $.ajax({
        url: 'file-upload.php',  //Server script to process data
        type: 'POST',
        xhr: function() {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload){ // Check if upload property exists
                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
            }
            return myXhr;
        },
        //Ajax events
        beforeSend: beforeSendHandler,
        success: completeHandler,
        error: errorHandler,
        // Form data
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    });
});


function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}
    </script>-->



    <script>
    	inicio = 10;
		fim = 10;
    	
    	$(window).scroll(function() {
		   //if($(".scroll5").scrollTop() + $(".scroll5").height() > $(document).height() ) {//- 100
		   	if($(window).scrollTop() == $(document).height() - $(window).height()){
		       //alert("near bottom!");
		       //var qnt = "";
		     	var qnt = $('#dadosHandler').children().length;
                var rowS = "<?php echo getRowDrinks(); ?>";
		        if(qnt < rowS){
		        	$('div#loadmoreajaxloader').show();
			       	$.ajax({
				      url: "handler.php",
				      type: "post",
				      data: {inicio: inicio, fim : fim},
				      success: function(data){
				           //$("#formDelImg").append(data);
				           inicio = inicio + 10;
				           //fim = fim + 10;
				           //alert(inicio + "valore"+fim);TODO TRATAR QUANTIDADE PRA N PRECISAR CHAMAR
				           
				           //alert(inicio);
				           	
				           function finishLoad(){
							  $('div#loadmoreajaxloader').hide();
							  $("#dadosHandler").append(data);
							}
							setTimeout(finishLoad, 1500);
				           
				           
				      },
				      error:function(){
				          alert("failure");
				          //$("#result").append('Error Occurred while fetching data.');
				      }   
				    }); 
		        }
		       	
		       //}
		   }
		});



    </script>
	<script src="../lightbox/src/js/lightbox.js"></script>
	<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
     
    })
</script>
</body>
</html>