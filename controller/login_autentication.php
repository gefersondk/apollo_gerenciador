<?php

	include_once 'conexao.php';
	include_once 'functionsPDO.php';
	
	header("Content-Type: text/html; charset=utf-8",true);
	
	//session_start();
	


	$usuario = $_POST['term_login'];
	$senha = $_POST['term_password'];

	
	
	if(verificarUsuario($usuario,$senha)){
		
		$_SESSION["logado"]=true;
		$_SESSION["login"]=$usuario;

		//header("Location:admin");
		echo "<script>window.location.href='admin'</script>";
		
		//header("Location:principal.php");//pagina Inicial depois de autorizado
	}
	else{
		
		$_SESSION["logado"] = false;
		header("Content-Type: text/html; charset=utf-8",true);

		session_unset();
		session_destroy();//a

	
		//header("Location:login?login=0");//Volta a pagina de login
		echo "<div class='alert alert-warning' role='alert'>Usuario e/ou senha invalidos</div>";

	}
?>