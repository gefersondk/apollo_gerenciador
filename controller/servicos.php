<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }

   include_once "functionsGerenciadorConteudoPDO.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/style_admin.css">
	<style>
		
		.btn_servico{
			margin: 0.5em;
		}
	</style>

</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li><a href="upload">Galeria</a></li><!--galeria_upload/upload.php-->
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br" target="_blank">Ver Site</a></li>
				      	<li><a href="password_change">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12"><a href="admin" class="pull-right" style="padding:11px;"><button class="button-back">Voltar</button></a></div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="panel panel-default">

					  <div class="panel-heading">
					  	
					    <h3 class="panel-title"><a href="novo_servico"><button class="button-add">Adicionar Serviço</button></a></h3>
					    

					  </div>
					  <div class="panel-body">
					    
							<table class="table table-striped">
							    <!--<thead>
							      <tr>
							        <th>Apollo Administração</th>
							        <th class="text-right">Ação</th>
							
							      </tr>
							    </thead>-->
							    <tbody>
						    	  <?php
						    	  	$servicos = getAllServicos();

						    	  	if(isset($servicos)){

						    	  			foreach ($servicos as $key => $value) {
						    	  				echo "<tr>";
											    echo    "<td>". $value->titulo."</td>";
											  
											        
											    echo    "<td><form action='servico_apagar' method='Post'><button type='submit' value='".$value->id."' name='delete' class='btn btn-danger pull-right btn_servico'>apagar</button></form><form action='servico_editar' method='Post'><button type='submit' value='".$value->id."' name='edit' class='btn btn-primary pull-right btn_servico'>modificar</button></form></td>";
										      	echo "</tr>";
						    	  			}

						    	  	}

						    	  ?>
							      

							      
							     

							    </tbody>
							</table>

	  				</div>
				</div>

			</div>
		</div>
	</div>
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

</body>
</html>