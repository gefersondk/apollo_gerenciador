<?php
  session_start();
  //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
 
    //header("Content-Type: text/html; charset=utf-8",true);
  if (!isset($_SESSION["logado"]) && (!($_SESSION["logado"])))
   {
      header("Location:login");
   }
    include_once "functionsGerenciadorConteudoPDO.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Painel de administração</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../css/style_admin.css">
	<script src="../ckeditor/ckeditor.js"></script>


	<style>

		.title_banner{
			color: #6c6360;
			font-size: 2em;
		}

	</style>

</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Apollo</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="admin">Home <span class="sr-only">(current)</span></a></li>
				        <li><a href="upload">Galeria</a></li>
				        
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="http://www.apollobartenders.com.br" target="_blank">Ver Site</a></li>
				      	<li><a href="password_change">Alterar Senha</a></li>
				        <li><a href="logout">Sair</a></li>
				        
				      </ul>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12"><a href="admin" class="pull-right" style="padding:11px;"><button class="button-back">Voltar</button></a></div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="result"></div>
				<div class="panel panel-default">
					  <div class="panel-heading">
					    <h3 class="panel-title">Sobre</h3>
					  </div>
					  <div class="panel-body">
					    
							<table class="table table-striped">
							    
							    <tbody>
							      <tr>
							       
							        <td>
							        	<?php 
							        		$dados = getAbout();
							        	?>
							        	<h5 class="title_banner">Apresentação</h5>
										<form  role="form" id="formUpdateAbout" action="about_edit_pro">
								            <textarea  class="ckeditor" id="apresentacao" rows="10" cols="80" name="apresentacao" >
								                <?php echo $dados[0]->texto_de_apresentacao?>
								            </textarea>
								      

								            <h5 class="title_banner">Sobre</h5>
											<textarea  id="editor2" rows="10" cols="80" name="valores" >
								                <?php echo $dados[0]->texto_valores?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor2' );
								            </script>

								            <h5 class="title_banner">Missão, Visão, Valores</h5>
											<textarea  id="editorM" rows="10" cols="80" name="missao" >
								                <?php echo $dados[0]->texto_missao?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editorM' );
								            </script>
											
											<h5 class="title_banner">Fundadores</h5>
											<textarea  id="editor3" rows="10" cols="80" name="fundadores" >
								                <?php echo $dados[0]->texto_fundadores?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor3' );
								            </script>

											<h5 class="title_banner">Plano de Fidelidade</h5>
											<textarea  id="editor4" rows="10" cols="80" name="planoDeFidelidade" >
								                <?php echo $dados[0]->texto_plano_de_fidelidade?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'editor4' );
								            </script>

											<h5 class="title_banner">Como Contratar</h5>
											<textarea  id="como_contratar" rows="10" cols="80" name="comoContratar" >
								                <?php echo $dados[0]->texto_como_contratar?>
								            </textarea>
								            <script>
								                // Replace the <textarea id="editor1"> with a CKEditor
								                // instance, using default configuration.
								                CKEDITOR.replace( 'como_contratar' );
								            </script>
											<br>
											<label for="link_video_explicativo" class="title_banner">Incorporar Video</label>
											 <input  type="text"  id="link_video_explicativo" class="form-control" placeholder="insira codigo do video de explicação" name="link" value='<?php echo $dados[0]->url_youtube_video_explicativo; ?>'>

								            <button type="submit" class="button-modify pull-right">Salvar</button>
								        </form>

							        </td>
							
							      </tr>

							    </tbody>
							</table>

	  				</div>
				</div>

			</div>
		</div>
	</div>
	
	<!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
	
	<script>
		

		$( "#formUpdateAbout" ).submit(function( event ) {

		  // Stop form from submitting normally
		  event.preventDefault();
		 
		  for ( instance in CKEDITOR.instances )
        	CKEDITOR.instances[instance].updateElement();

		  // Get some values from elements on the page:
		  var $form = $( this ),
		    apresentacao = $form.find( "textarea[name='apresentacao']" ).val(),
		    valores = $form.find( "textarea[name='valores']" ).val(),
		    missao = $form.find( "textarea[name='missao']" ).val(),
		    fundadores = $form.find( "textarea[name='fundadores']" ).val(),
		    planoDeFidelidade = $form.find( "textarea[name='planoDeFidelidade']" ).val(),
		    comoContratar= $form.find( "textarea[name='comoContratar']" ).val(),
		    link = $form.find( "input[name='link']" ).val(),
		    url = $form.attr( "action" );
		
		  // Send the data using post
		  var posting = $.post( url, { apresentacao: apresentacao, valores: valores, missao: missao, fundadores: fundadores, planoDeFidelidade: planoDeFidelidade, comoContratar: comoContratar, link: link } );
		 
		  // Put the results in a div
		  posting.done(function( data ) {

		    //var content = $( data ).find( "#content" );

		    //if(data.localeCompare("session_error2")){//
		    	//window.location.reload(); 
		    	
		    //}else{
		    	$( "#result" ).empty().append( data );
		    	//$( "#result" ).empty().append( data.localeCompare('session_error') );
		    	//$("#formUpdatePassword").trigger('reset');


		    //}
		    function relload(){
			  window.location = "admin";
			}
			setTimeout(relload, 500);
		  });
		});

    </script>

</body>
</html>