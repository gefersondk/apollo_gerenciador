<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
	<script>
		$('.grid').masonry({
		  // options...
		  itemSelector: '.grid-item',
		  columnWidth: 200
		});
	</script>
	
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/masonry_style.css">

	
</head>
<body>

	<?php include_once "controller/functionsGerenciadorConteudoPDO.php"; ?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="index">APOLLO</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index">Inicio</a>
                    </li>

                    <li>
                        <a href="index#sobre">Sobre</a>
                    </li>

                    <li>
                        <a href="index#services">Serviços</a>
                    </li>
                    <li>
                        <a href="index#contact">Contato</a>
                    </li>
                    
                    <li>
                        <a href="drinks">Galeria</a>
                    </li>

                    <li><a href="midias_sociais">Mídias Sociais</a></li>

                    

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <div class="container-fluid">
        
        <div class="row">
            <?php 

                $imgheader = getBannerGaleriaM();
                if(isset($imgheader)){
                    $path_img_header = substr($imgheader[0]->path_imagem, 3);
                }

            ?>
            <div class="col-lg-12 header_midia" style="background: url(<?php echo $path_img_header; ?>) no-repeat center center;">
                
                <div class="col-lg-12">
                    <div class="container">

                        <h1 class="intro-message_midia">Mídias Sociais</h1>
                    </div>
                </div>

            </div>

        </div>

    </div>


	<div class="wrapper">

		<div class="masonry">
			
			<div id="dadosHandler">
				
				<?php

                $midias = getMidiasScroll(0,50);
                if(isset($midias)){
                    foreach ($midias as $key => $value) {
                    echo "<div class='item'>
                
                        $value->embed
                    
                    </div>";
                }
                }
                

            ?>

			</div>

			
			
		</div>
	</div>	

	
<div id="loadmoreajaxloader" style="display:none;"><center><img src="img/spinner4.gif" style="width:5em;" /></center></div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <script>
        inicio = 10;
        fim = 10;
        $(window).scroll(function () {  
                  if($(window).scrollTop() == $(document).height() - $(window).height()){
                    var qnt = $('#dadosHandler').children().length;;
                    //alert(qnt);
                    var qnt_lm = "<?php echo getRowMidias_Sociais(); ?>";
                    if ( qnt < qnt_lm) {
                        
                        $('div#loadmoreajaxloader').show();
                            $.ajax({
                              url: "controller/handlerMidia.php",
                              type: "post",
                              data: {inicio: inicio, fim : fim},
                              success: function(data){
                                   //$("#formDelImg").append(data);

                                   inicio = inicio +10;
                                   //fim = fim + 10;
                                   //alert(inicio + "valore"+fim);TODO TRATAR QUANTIDADE PRA N PRECISAR CHAMAR
                                   
                                   //alert(inicio);
                                    
                                   function finishLoad(){
                                      $('div#loadmoreajaxloader').hide();
                                      $("#dadosHandler").append(data);
                                    }
                                    setTimeout(finishLoad, 1500);
                                   
                                   
                              },
                              error:function(){
                                $('div#loadmoreajaxloader').hide();
                                  alert("failure");
                                  //$("#result").append('Error Occurred while fetching data.');
                              }   
                            }); 
                   
                    }
                            
                  }
        });
    </script>

    


</body>

</html>
