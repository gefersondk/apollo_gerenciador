<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Apollo Bartenders</title>

	<link rel="stylesheet" href="masonry/css/masonry.css">
<link rel="stylesheet" href="css/bootstrap.css">

<!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/midia_s.css">

    <link rel="stylesheet" type="text/css" href="css/base_style_apollo.css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/logo-nav.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="icon" href="img/fviconapollo/apolloLogoFI.png">
	
</head>



<body>
    <?php include_once "controller/functionsGerenciadorConteudoPDO.php"; ?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav visible-xs-block visible-sm-block" href="index" id="ApolloLMenu">APOLLO</a>
                <a class="navbar-brand topnav visible-md-block visible-lg-block" href="index" ><img src="img/apolloLogo3.png" alt="" style="width:7em;" id="imgLogo"></a>
                
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" id="ulNavApollo">
                    
                    <li>
                        <a href="index#services">SERVIÇOS</a>
                    </li>

                    
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">COMO CONTRATAR?</a>
                      <ul class="dropdown-menu">
                        <li><a href="como_contratar#comoContratar">Escolhendo uma empresa</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="como_contratar#formOrcamentoS">Solicite seu orçamento</a></li>                
                      </ul>
                    </li>

                    <li>
                        <a href="plano#plano">PLANO DE FIDELIDADE</a>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">A EMPRESA <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="empresa#sobre">Sobre</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="empresa#missao">Missão, Visão, Valores</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="empresa#fundadores">Fundadores</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="empresa#fale">Fale Conosco</a></li>                
                      </ul>
                    </li>

                    <li>
                        <a href="drinks">GALERIA</a>
                    </li>

                    <li>
                        <a href="empresa#fale">FALE CONOSCO</a>
                    </li>

                    <li><a href="midias_sociais">MÍDIAS SOCIAIS</a></li>

                    

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container-fluid">
        
        <div class="row">
            <?php 

                $imgheader = getBannerGaleriaM();
                if(isset($imgheader)){
                    $path_img_header = substr($imgheader[0]->path_imagem, 3);
                }

            ?>
            <div class="col-lg-12 header_midia" style="background: url(<?php echo $path_img_header; ?>) no-repeat center center;">
                
                <div class="col-lg-12">
                    <div class="container">

                        <h1 class="intro-message_midia">Mídias Sociais</h1>
                    </div>
                </div>

            </div>

        </div>

    </div>
	
	<div class="container" id="conteudo_midia">
		<div class="row">
			<div class="col-md-12">
				<div class="grid">
				  <!--<div class="grid-item"></div>-->
				  <!--<div class="grid-item grid-item--width2 grid-item--height2"></div>-->
				  <!--<div class="grid-item grid-item--height3"></div>-->
				  
				  	
				  	<div id="dadosHandler">
						<?php

			                $midias = getMidiasScroll(0,20);
			                if(isset($midias)){
			                    foreach ($midias as $key => $value) {
			                    echo "<div class='grid-item' >
			                
			                        $value->embed
			                    
			                    </div>";
		                		}
		                	}	        
	            		?>
					

				  </div>
				</div>
			</div>
		</div>
	</div>

	
	<div id="loadmoreajaxloader" style="display:none;"><center><img src="img/spinner4.gif" style="width:5em;" /></center></div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!--
    <script>
        inicio = 10;
        fim = 10;
        $(window).scroll(function () {  
                  if($(window).scrollTop() == $(document).height() - $(window).height()){
                    var qnt = $('#dadosHandler').children().length;;
                    //alert(qnt);
                    var qnt_lm = "<?php echo getRowMidias_Sociais(); ?>";
                    if ( qnt < qnt_lm) {
                        
                        $('div#loadmoreajaxloader').show();
                            $.ajax({
                              url: "controller/handlerMidia.php",
                              type: "post",
                              data: {inicio: inicio, fim : fim},
                              success: function(data){
                                   //$("#formDelImg").append(data);

                                   inicio = inicio +10;
                                   //fim = fim + 10;
                                   //alert(inicio + "valore"+fim);TODO TRATAR QUANTIDADE PRA N PRECISAR CHAMAR
                                   
                                   //alert(inicio);
                                    
                                   function finishLoad(){
                                      $('div#loadmoreajaxloader').hide();
                                      $("#dadosHandler").append(data);
                                    }
                                    setTimeout(finishLoad, 1500);
                                   
                                   
                              },
                              error:function(){
                                $('div#loadmoreajaxloader').hide();
                                  alert("failure");
                                  //$("#result").append('Error Occurred while fetching data.');
                              }   
                            }); 
                   
                    }
                            
                  }
        });-->
    </script>



	

	<!--<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>-->
	<!--<script src='http://masonry.desandro.com/masonry.pkgd.js'></script>-->
	<script src="masonry/js/jquery.min.js"></script>
	<script src="masonry/js/masonry.pkgd.js"></script>
	<script>
		// external js: masonry.pkgd.js

		$(document).ready( function() {

		  $('.grid').masonry({
		    itemSelector: '.grid-item',
		    columnWidth: 160
		  });
		  
		});
	</script>


    


</body>
</html>