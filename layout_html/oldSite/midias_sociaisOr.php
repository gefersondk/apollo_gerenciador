<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>apollo - bartenders</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/midia_s.css">

    <link rel="stylesheet" type="text/css" href="css/base_style_apollo.css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<style>
    iframe{
  width: 25em;


  border-radius: 5px;
}


</style>
<body>
    <?php include_once "controller/functionsGerenciadorConteudoPDO.php"; ?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="index">APOLLO</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index">Inicio</a>
                    </li>

                    <li>
                        <a href="index#sobre">Sobre</a>
                    </li>

                    <li>
                        <a href="index#services">Serviços</a>
                    </li>
                    <li>
                        <a href="index#contact">Contato</a>
                    </li>
                    
                    <li>
                        <a href="drinks">Galeria</a>
                    </li>

                    <li><a href="midias_sociais">Mídias Sociais</a></li>

                    

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container-fluid">
        
        <div class="row">
            <?php 

                $imgheader = getBannerGaleriaM();
                if(isset($imgheader)){
                    $path_img_header = substr($imgheader[0]->path_imagem, 3);
                }

            ?>
            <div class="col-lg-12 header_midia" style="background: url(<?php echo $path_img_header; ?>) no-repeat center center;">
                
                <div class="col-lg-12">
                    <div class="container">

                        <h1 class="intro-message_midia">Mídias Sociais</h1>
                    </div>
                </div>

            </div>

        </div>

    </div>


    <div class="container" id="container_media">
      <!--<h1>Masonry CSS with Bootstrap</h1>-->

        <div class="menu rowMod" id="dadosHandler">
            
            <?php

                $midias = getMidiasScroll(0,30);
                if(isset($midias)){
                    foreach ($midias as $key => $value) {
                    echo "<div class='menu-category'>
                
                        $value->embed
                    
                    </div>";
                }
                }
                

            ?>

            

        </div>
    </div>
    <div id="loadmoreajaxloaderStart" style="display:none;padding-top:10em;"><center><img src="img/spinner4.gif" style="width:5em;" /></center></div>
    <div id="loadmoreajaxloader" style="display:none;"><center><img src="img/spinner4.gif" style="width:5em;" /></center></div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!--
    <script>
        inicio = 10;
        fim = 10;
        $(window).scroll(function () {  
                  if($(window).scrollTop() == $(document).height() - $(window).height()){
                    var qnt = $('#dadosHandler').children().length;;
                    //alert(qnt);
                    var qnt_lm = "<?php echo getRowMidias_Sociais(); ?>";
                    if ( qnt < qnt_lm) {
                        
                        $('div#loadmoreajaxloader').show();
                            $.ajax({
                              url: "controller/handlerMidia.php",
                              type: "post",
                              data: {inicio: inicio, fim : fim},
                              success: function(data){
                                   //$("#formDelImg").append(data);

                                   inicio = inicio +10;
                                   //fim = fim + 10;
                                   //alert(inicio + "valore"+fim);TODO TRATAR QUANTIDADE PRA N PRECISAR CHAMAR
                                   
                                   //alert(inicio);
                                    
                                   function finishLoad(){
                                      $('div#loadmoreajaxloader').hide();
                                      $("#dadosHandler").append(data);
                                    }
                                    setTimeout(finishLoad, 1500);
                                   
                                   
                              },
                              error:function(){
                                $('div#loadmoreajaxloader').hide();
                                  alert("failure");
                                  //$("#result").append('Error Occurred while fetching data.');
                              }   
                            }); 
                   
                    }
                            
                  }
        });
    </script>
    -->
    
    <script>
   
        $('#container_media').hide();
        $(document).ready(function(){
            $('#loadmoreajaxloaderStart').show();
        });
        $(window).load(function(){
            $('#loadmoreajaxloaderStart').hide();
        $('#container_media').show();
            
        });
    </script>


</body>

</html>
