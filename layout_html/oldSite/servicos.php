<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>apollo - bartenders</title>

    <link rel="stylesheet" type="text/css" href="lightbox/src/css/lightbox.css">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/midia_s.css">

     <!-- Custom CSS -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/gallery_apollo.css">

    <link rel="stylesheet" type="text/css" href="css/base_style_apollo.css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php include_once "controller/functionsGerenciadorConteudoPDO.php"; ?>
</head>

<body id="image_b" style="padding-top:0px;">
    
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="index">APOLLO</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index">Inicio</a>
                    </li>

                    <li>
                        <a href="index#sobre">Sobre</a>
                    </li>

                    <li>
                        <a href="index#services">Serviços</a>
                    </li>
                    <li>
                        <a href="index#contact">Contato</a>
                    </li>
                    
                    <li>
                        <a href="drinks">Galeria</a>
                    </li>

                    <li><a href="midias_sociais">Mídias Sociais</a></li>

                    

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container-fluid">
        
        <div class="row">
            <?php 

                $imgheader = getBannerGaleriaM();
                if(isset($imgheader)){
                    $path_img_header = substr($imgheader[0]->path_imagem, 3);
                }

            ?>
            <div class="col-lg-12 header_midia" style="background: url(<?php echo $path_img_header; ?>) no-repeat center center;">
                
                <div class="col-lg-12">
                    <div class="container">

                        <h1 class="intro-message_midia">Galeria</h1>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            

            <div id="menu_tabs_galley"><!--inicio-->              
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="drinks" >Drinks</a></li>
                    <li role="presentation" class="active"><a href="servicos" >Serviço</a></li>
                    <li role="presentation"><a href="figurinos" >Figurino</a></li>
                </ul>
                
                <div role="tabpanel" class="tab-pane active" id="drinks">
                                <!--inicio conteudo-->
                                <br>

                                <div id="dadosHandler">
                                    <?php 

                                    $servicos_img = getAllImageServicosScrollWeb(0,10);
                                    
                                    if(isset($servicos_img)){
                                        foreach ($servicos_img as $key => $value) {
                                        $path_img_servico = substr($value->path_imagem, 3);
                                        $path_img_servico_thumbnail = substr($value->path_imagem_thumbnail, 3);

                                        echo "<div class='col-lg-3 col-md-4 col-xs-6 thumb'>    
                                        <a class='thumbnail img-responsive' href='".$path_img_servico."' data-lightbox='drink' data-title='Apollo'><img src='".$path_img_servico_thumbnail."' alt='' class='gallery_img'></a></div>";
                                    }
                                    }
                                    

                                    ?>
                                </div>
                                
                                
                                
                                
                                <!--fim conteudo-->

                    </div>

            </div><!--fim--> 
            
        </div>

        <hr>


    </div>
    <!-- /.container -->
    
    
    <?php
        $data_footer = getRodape();
        $path_img_footer = substr($data_footer[0]->path_imagem, 3);
     ?>


    <div class="banner" style="background: url(<?php echo $path_img_footer; ?>) no-repeat center center;"> 

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <?php echo $data_footer[0]->texto_rodape1; ?>
                    <h2><?php echo $data_footer[0]->texto_rodape2; ?></h2>                  
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="<?php echo $data_index[0]->link_youtube; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-youtube"></i> <span class="network-name">Youtube</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $data_index[0]->link_facebook; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-facebook"></i> <span class="network-name">Facebook</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $data_index[0]->link_instagram; ?>" class="btn btn-default btn-lg" target="_blank"><i class="fa fa-fw fa-instagram"></i> <span class="network-name">Instagram</span></a>
                        </li>
                    </ul>


                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->


    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="index">Inicio</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="index#sobre">Sobre</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="index#services">Serviços</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="index#contact">Contato</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; Apollo 2015. Todos Direitos Reservados</p>
                </div>
            </div>
        </div>
    </footer>
    <div id="loadmoreajaxloader" style="display:none;"><center><img src="img/spinner4.gif" style="width:5em;" /></center></div>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <script>
        inicio = 10;
        fim = 10;
        $(window).scroll(function () {  
                  if($(window).scrollTop() == $(document).height() - $(window).height()){
                    var qnt = $('#dadosHandler').children().length;
                    var rowS = "<?php echo getRowServicosScrollWeb(); ?>";
                    //alert(qnt);
                    //alert(rowS);
                    if(qnt < rowS){
                        $('div#loadmoreajaxloader').show();
                            $.ajax({
                              url: "controller/handlerServicoWeb.php",
                              type: "post",
                              data: {inicio: inicio, fim : fim},
                              success: function(data){
                                   //$("#formDelImg").append(data);

                                   inicio = inicio +10;
                                   //fim = fim + 10;
                                   //alert(inicio + "valore"+fim);TODO TRATAR QUANTIDADE PRA N PRECISAR CHAMAR
                                   
                                   //alert(inicio);
                                    
                                   function finishLoad(){
                                      $('div#loadmoreajaxloader').hide();
                                      $("#dadosHandler").append(data);
                                    }
                                    setTimeout(finishLoad, 1500);
                                   
                                   
                              },
                              error:function(){
                                $('div#loadmoreajaxloader').hide();
                                  alert("failure");
                                  //$("#result").append('Error Occurred while fetching data.');
                              }   
                            }); 
                    }
                   
                    
                            
                  }
        });
    </script>

    <script src="lightbox/src/js/lightbox.js"></script>

    <!--<script>
        $('#tabService').click(function (e) {
        
            $('#drinks').hide()
            $('#service').show()
            
        });

        $('#tabDrinks').click(function (e) {
        
            $('#service').hide()
            $('#drinks').show()
            
        });

    </script>-->

    

    <script>
        lightbox.option({
          'resizeDuration': 100,
          'wrapAround': true,
        })
    </script>

    

</body>

</html>
